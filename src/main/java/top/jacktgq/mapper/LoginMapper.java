package top.jacktgq.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import top.jacktgq.pojo.UserInfo;

/**
 * @Author zhourenxi
 * @Date 2020/4/13
 * @Description 用户登录的DAO层接口
 */
@Repository
public interface LoginMapper {

    //用户登录验证
    UserInfo login(@Param("userInfo") UserInfo userInfo);
}

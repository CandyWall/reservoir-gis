package top.jacktgq.mapper;

import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author CandyWall
 * @Date 2020/4/14--11:42
 * @Description 水库信息管理的DAO层接口
 */
@Repository
public interface ReservoirMapper {
    //查询水库的坐标集
    List<String> getResevoirGeom();
}

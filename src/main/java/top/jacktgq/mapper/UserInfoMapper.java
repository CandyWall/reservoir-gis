package top.jacktgq.mapper;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import top.jacktgq.pojo.UserInfo;

import java.util.List;
import java.util.Map;

/**
 * @Author zhourenxi
 * @Date 2020/4/13
 * @Description 用户管理的DAO层接口
 */
@Repository
public interface UserInfoMapper {
    //查询用户信息总条数
    int getTotalCount();

    //查询用户信息并分页
    List<UserInfo> getUserInfos(Map<String, Object> paramsMap);

    //添加用户
    void addUser(@Param("userInfo") UserInfo userInfo);

    //修改密码
    int updatePassword(@Param("new_password") String new_password, @Param("user_id") String user_id);

    //根据用户编号查询用户信息
    UserInfo getUserById(String u_id);

    //根据用户编号，修改用户信息
    void updateUserById(UserInfo userInfo);

    //根据用户编号删除用户信息
    void delUserById(String u_id);
}

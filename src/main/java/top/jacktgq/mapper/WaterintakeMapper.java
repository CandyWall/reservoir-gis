package top.jacktgq.mapper;

import org.springframework.stereotype.Repository;
import top.jacktgq.pojo.Waterintake;

import java.util.List;
import java.util.Map;

/**
 * @Author zhourenxi
 * @Date 2020/4/13
 * @Description 取水口管理的DAO层接口
 */
@Repository
public interface WaterintakeMapper {
    //查询取水口的总条数
    int getTotalCount();

    //查询取水口信息并分页
    List<Waterintake> getWaterIntakeInfos(Map<String, Object> paramsMap);

    //查询取水口的坐标集
    List<String> getWaterintakeGeom();

    //新增取水口
    void addWaterintake(Waterintake waterintake);

    //根据取水口编号查询取水口信息
    Waterintake getWaterintakeById(Integer w_id);

    //根据取水口编号，修改取水口信息
    void updateWaterintakeById(Waterintake waterintake);

    //根据取水口编号删除取水口信息
    void delWaterIntakeById(Integer w_id);
}

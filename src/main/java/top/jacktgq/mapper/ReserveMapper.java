package top.jacktgq.mapper;

import org.springframework.stereotype.Repository;
import top.jacktgq.pojo.Reserve;

import java.util.List;
import java.util.Map;

/**
 * @Author zhourenxi
 * @Date 2020/4/13
 * @Description 保护区管理的DAO层接口
 */
@Repository
public interface ReserveMapper {

    //查询保护区的总条数
    int getTotalCount();

    //查询保护区信息并分页
    List<Reserve> getReserveInfos(Map<String, Object> paramsMap);

    //查询保护区的坐标集
    List<String> getReserveGeom();

    //添加保护区
    void addReserve(Reserve reserve);

    //统计各个保护区的面积
    List<Map<String, Object>> areaStatistics();

    //根据保护区编号查询保护区信息
    Reserve getReserveById(Integer res_id);

    //根据保护区编号，修改保护区信息
    void updateReserveById(Reserve reserve);

    //根据保护区编号删除保护区信息
    void delReserveById(Integer res_id);
}

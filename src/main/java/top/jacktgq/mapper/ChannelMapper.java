package top.jacktgq.mapper;

import org.springframework.stereotype.Repository;
import top.jacktgq.pojo.Channel;

import java.util.List;
import java.util.Map;

/**
 * @Author CandyWall
 * @Date 2020/4/12--17:33
 * @Description 渠道的DAO层接口
 */
@Repository
public interface ChannelMapper {
    void addChannel(Channel channel);

    //查询渠道的总条数
    int getTotalCount();

    //查询渠道信息并分页
    List<Channel> getChannelsByPage(Map<String, Object> paramsMap);

    //查询渠道的坐标集
    List<String> getChannelGeom();

    //统计各个渠道的长度
    List<Map<String, Object>> lengthStatistics();

    //根据渠道编号查询渠道信息
    Channel getChannelById(Integer c_id);

    //根据渠道编号，修改渠道信息
    void updateChannelById(Channel channel);

    //根据渠道编号删除渠道信息
    void delChannelById(Integer c_id);
}

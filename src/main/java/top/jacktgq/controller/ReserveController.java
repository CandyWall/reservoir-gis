package top.jacktgq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.jacktgq.pojo.Channel;
import top.jacktgq.pojo.PageBean;
import top.jacktgq.pojo.Reserve;
import top.jacktgq.service.ReserveService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author CandyWall
 * @Date 2020/4/12--16:00
 * @Description 保护区前端控制器
 */
@Controller
public class ReserveController {

    @Autowired
    ReserveService reserveService;

    @RequestMapping("addReserve")
    //添加保护区
    public @ResponseBody Map<String, Object> addReserve(Reserve reserve) {
        System.out.println(reserve);
        reserveService.addReserve(reserve);
        Map<String, Object> resultParams = new HashMap<String, Object>();
        resultParams.put("ifSucceed", true);
        return resultParams;
    }

    //查询保护区信息并分页显示
    @RequestMapping("/getReserveInfos")
    public @ResponseBody
    Map<String, Object> getReserveInfos(Integer currentPage,       //当前页序号
                                          Integer currentCount) {    //当前页显示的条数) {
        //如果首次访问，当前页为空，置为1
        if (currentPage == null) {
            currentPage = 1;
        }
        //调用业务层分页查询所有的用户信息
        PageBean<Reserve> pageBean = reserveService.getReserveInfos(currentPage, currentCount);
        Map<String, Object> pageBeanMap = new HashMap<String, Object>();
        pageBeanMap.put("status", 200);
        pageBeanMap.put("hint", "连接成功");
        pageBeanMap.put("total", pageBean.getTotalCount());
        pageBeanMap.put("rows", pageBean.getList());
        System.out.println("getEquipments方法返回值========");
        System.out.println(pageBeanMap);
        System.out.println("getEquipments方法返回值========");
        return pageBeanMap;
    }

    @RequestMapping("/getReserveGeom")
    public void getReserveGeom(HttpServletResponse response) throws IOException {
        //查询所有的水库信息
        List<String> features = reserveService.getReserveGeom();

        //将坐标数据拼成geojson格式
        String geoJson = "{\"type\": \"FeatureCollection\",\"crs\": {\"type\": \"name\",\"properties\": {\"name\": \"EPSG:4326\"}},\"features\":";
        geoJson += features + "}";
        response.getWriter().write(geoJson);
    }

    @RequestMapping("/areaStatistics")
    //统计各个保护区的面积
    public @ResponseBody List<Map<String, Object>> areaStatistics() {
        return reserveService.areaStatistics();
    }

    //修改渠道数据的回显
    @RequestMapping("/showUpdateReserve")
    public String showUpdateReserve(Integer res_id, Model model) {
        System.out.println(res_id);
        Reserve reserve = reserveService.getReserveById(res_id);
        model.addAttribute("reserve", reserve);
        return "/common/updateReserve";
    }

    //根据保护区编号，修改保护区信息
    @RequestMapping("/updateReserveById")
    public @ResponseBody Map<String, Object> updateReserveById(Reserve reserve) {
        reserveService.updateReserveById(reserve);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("ifSucceed", true);
        return resultMap;
    }

    //根据保护区编号删除保护区信息
    @RequestMapping("/delReserveById")
    public @ResponseBody Map<String, Object> delReserveById(Integer res_id) {
        System.out.println("保护区编号为：" + res_id);
        reserveService.delReserveById(res_id);

        Map<String, Object> resultParams = new HashMap<String, Object>();
        resultParams.put("ifSucceed", true);
        return resultParams;
    }
}

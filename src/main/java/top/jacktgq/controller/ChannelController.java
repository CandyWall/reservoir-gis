package top.jacktgq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.jacktgq.pojo.Channel;
import top.jacktgq.pojo.PageBean;
import top.jacktgq.pojo.Waterintake;
import top.jacktgq.service.ChannelService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author CandyWall
 * @Date 2020/4/12--16:00
 * @Description 渠道前端控制器
 */
@Controller
public class ChannelController {
     @Autowired
     ChannelService channelService;

    @RequestMapping("addChannel")
    public @ResponseBody
    Map<String, Object> addChannel(Channel channel) {
        System.out.println(channel);
        channelService.addChannel(channel);
        Map<String, Object> resultParams = new HashMap<String, Object>();
        resultParams.put("ifSucceed", true);
        return resultParams;
    }

    //查询渠道信息并分页显示
    @RequestMapping("/getChannelsByPage")
    public @ResponseBody Map<String, Object> getChannelsByPage(Integer currentPage,       //当前页序号
                                                                 Integer currentCount) {    //当前页显示的条数) {
        //如果首次访问，当前页为空，置为1
        if (currentPage == null) {
            currentPage = 1;
        }
        //调用业务层分页查询所有的用户信息
        PageBean<Channel> pageBean = channelService.getChannelsByPage(currentPage, currentCount);
        Map<String, Object> pageBeanMap = new HashMap<String, Object>();
        pageBeanMap.put("status", 200);
        pageBeanMap.put("hint", "连接成功");
        pageBeanMap.put("total", pageBean.getTotalCount());
        pageBeanMap.put("rows", pageBean.getList());
        System.out.println("getEquipments方法返回值========");
        System.out.println(pageBeanMap);
        System.out.println("getEquipments方法返回值========");
        return pageBeanMap;
    }

    @RequestMapping("/getChannelGeom")
    public void getChannelGeom(HttpServletResponse response) throws IOException {
        //查询渠道的坐标集
        List<String> features = channelService.getChannelGeom();

        //将坐标数据拼成geojson格式
        String geoJson = "{\"type\": \"FeatureCollection\",\"crs\": {\"type\": \"name\",\"properties\": {\"name\": \"EPSG:4326\"}},\"features\":";
        geoJson += features + "}";
        response.getWriter().write(geoJson);
    }

    @RequestMapping("/lengthStatistics")
    //统计各个渠道的长度
    public @ResponseBody List<Map<String, Object>> lengthStatistics() {
        return channelService.lengthStatistics();
    }

    //修改渠道数据的回显
    @RequestMapping("/showUpdateChannel")
    public String showUpdateChannel(Integer c_id, Model model) {
        Channel channel = channelService.getChannelById(c_id);
        model.addAttribute("channel", channel);
        return "/common/updateChannel";
    }

    //根据渠道编号，修改渠道信息
    @RequestMapping("/updateChannelById")
    public @ResponseBody Map<String, Object> updateChannelById(Channel channel) {
        channelService.updateChannelById(channel);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("ifSucceed", true);
        return resultMap;
    }

    //根据渠道编号删除渠道信息
    @RequestMapping("/delChannelById")
    public @ResponseBody Map<String, Object> delChannelById(Integer c_id) {
        System.out.println("取水口编号为：" + c_id);
        channelService.delChannelById(c_id);

        Map<String, Object> resultParams = new HashMap<String, Object>();
        resultParams.put("ifSucceed", true);
        return resultParams;
    }
}

package top.jacktgq.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import top.jacktgq.pojo.PageBean;
import top.jacktgq.pojo.UserInfo;
import top.jacktgq.service.UserInfoService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;


/**
 * @Author zhourenxi
 * @Date 2020/4/13
 * @Description 用户操作前端控制器
 */
@Controller
public class UserController {
    @Autowired
    UserInfoService userInfoService;

    //添加用户
    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    public @ResponseBody Map<String,Object> addUser(UserInfo userInfo){

        Map<String, Object> resultMap = new HashMap<String, Object>();
        System.out.println("进来了"+userInfo);
        //执行添加操作
        userInfoService.addUser(userInfo);

        //查询用户并分页显示
        resultMap.put("ifSucceed", true);
        return resultMap;
    }

    //查询用户并分页显示
    @RequestMapping("/getUserInfos")
    public @ResponseBody Map<String, Object> getUserInfos(Integer currentPage,       //当前页序号
                                                          Integer currentCount) {    //当前页显示的条数) {
        //如果首次访问，当前页为空，置为1
        if (currentPage == null) {
            currentPage = 1;
        }
        //调用业务层分页查询所有的用户信息
        PageBean<UserInfo> pageBean = userInfoService.getUserInfos(currentPage, currentCount);
        Map<String, Object> pageBeanMap = new HashMap<String, Object>();
        pageBeanMap.put("status", 200);
        pageBeanMap.put("hint", "连接成功");
        pageBeanMap.put("total", pageBean.getTotalCount());
        pageBeanMap.put("rows", pageBean.getList());
        System.out.println("getEquipments方法返回值========");
        System.out.println(pageBeanMap);
        System.out.println("getEquipments方法返回值========");
        return pageBeanMap;
    }

    //更改用户密码
    @RequestMapping("/updatePassword")
    public @ResponseBody Map<String, Object> updatePassword(String old_password, String new_password, HttpSession session){
        UserInfo user = (UserInfo) session.getAttribute("user");
        String user_id = user.getU_id();

        Map<String, Object> resultMap = new HashMap<String, Object>();

        //判断原始密码是否正确
        if(old_password.equals(user.getU_password())){
            //执行修改操作
            userInfoService.updatePassword(new_password,user_id);
            System.out.println("成功");
            resultMap.put("ifSucceed", true);
        }else {
            System.out.println("失败");
            resultMap.put("ifSucceed", false);
        }
        return resultMap;
    }

    //回显用户信息到更改用户信息页面
    @RequestMapping("/showUpdateUser")
    public @ResponseBody Map<String, Object> showUpdateUser() {

        return null;
    }

    //更改用户信息
    @RequestMapping("/updateUser")
    public @ResponseBody Map<String, Object> updateUser() {

        return null;
    }

    //修改用户数据的回显
    @RequestMapping("/showUpdateUserForm")
    public String showUpdateUserForm(String u_id, Model model) {
        UserInfo userInfo = userInfoService.getUserById(u_id);
        model.addAttribute("userInfo", userInfo);
        return "/admin/updateUser";
    }

    //根据用户编号，修改用户信息
    @RequestMapping("/updateUserById")
    public @ResponseBody Map<String, Object> updateUserById(UserInfo userInfo) {
        userInfoService.updateUserById(userInfo);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("ifSucceed", true);
        return resultMap;
    }

    //根据用户编号删除用户信息
    @RequestMapping("/delUserById")
    public @ResponseBody Map<String, Object> delUserById(String u_id) {
        System.out.println("用户编号为：" + u_id);
       userInfoService.delUserById(u_id);

        Map<String, Object> resultParams = new HashMap<String, Object>();
        resultParams.put("ifSucceed", true);
        return resultParams;
    }
}

package top.jacktgq.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.jacktgq.service.ReservoirService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import	java.util.HashMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author CandyWall
 * @Date 2020/4/14--11:16
 * @Description 水库信息管理的前端控制器
 */
@Controller
public class ReservoirController {
    @Autowired
    ReservoirService reservoirService;

    @RequestMapping("/getReservoirGeom")
    public void getReservoirGeom(HttpServletResponse response) throws IOException {
        //查询水库的坐标集
        List<String> features = reservoirService.getResevoirGeom();

        //将坐标数据拼成geojson格式
        String geoJson = "{\"type\": \"FeatureCollection\",\"crs\": {\"type\": \"name\",\"properties\": {\"name\": \"EPSG:4326\"}},\"features\":";
        geoJson += features + "}";
        response.getWriter().write(geoJson);
    }
}

package top.jacktgq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import top.jacktgq.pojo.PageBean;
import top.jacktgq.pojo.Waterintake;
import top.jacktgq.service.WaterintakeService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author CandyWall   zhourenxi
 * @Date 2020/4/12--15:59    2020/4/13
 * @Description 渠道管理的前端控制器
 */
@Controller
public class WaterintakeController {

    @Autowired
    WaterintakeService waterintakeService;

    @RequestMapping("addWaterintake")
    public @ResponseBody Map<String, Object> addWaterintake(Waterintake waterintake) {
        System.out.println(waterintake);
        waterintakeService.addWaterintake(waterintake);
        Map<String, Object> resultParams = new HashMap<String, Object>();
        resultParams.put("ifSucceed", true);
        return resultParams;
    }

    //查询取水口信息并分页显示
    @RequestMapping("/getWaterIntakeInfos")
    public @ResponseBody Map<String, Object> getWaterIntakeInfos(Integer currentPage,       //当前页序号
                                                                Integer currentCount) {    //当前页显示的条数) {
        //如果首次访问，当前页为空，置为1
        if (currentPage == null) {
            currentPage = 1;
        }
        //调用业务层分页查询所有的用户信息
        PageBean<Waterintake> pageBean = waterintakeService.getWaterIntakeInfos(currentPage, currentCount);
        Map<String, Object> pageBeanMap = new HashMap<String, Object>();
        pageBeanMap.put("status", 200);
        pageBeanMap.put("hint", "连接成功");
        pageBeanMap.put("total", pageBean.getTotalCount());
        pageBeanMap.put("rows", pageBean.getList());
        System.out.println("getEquipments方法返回值========");
        System.out.println(pageBeanMap);
        System.out.println("getEquipments方法返回值========");
        return pageBeanMap;
    }

    @RequestMapping("/getWaterintakeGeom")
    public void getWaterintakeGeom(HttpServletResponse response) throws IOException {
        //查询取水口的坐标集
        List<String> features = waterintakeService.getWaterintakeGeom();

        //将坐标数据拼成geojson格式
        String geoJson = "{\"type\": \"FeatureCollection\",\"crs\": {\"type\": \"name\",\"properties\": {\"name\": \"EPSG:4326\"}},\"features\":";
        geoJson += features + "}";
        response.getWriter().write(geoJson);
    }

    @RequestMapping("/showUpdateWaterintake")
    //修改取水口数据的回显
    public String showUpdateWaterintake(Integer w_id, Model model) {
        Waterintake waterintake = waterintakeService.getWaterintakeById(w_id);
        model.addAttribute("waterintake", waterintake);
        return "/common/updateWaterintake";
    }

    @RequestMapping("/updateWaterintakeById")
    //根据取水口编号，修改取水口信息
    public @ResponseBody Map<String, Object> updateWaterintakeById(Waterintake waterintake) {
        System.out.println(waterintake);
        waterintakeService.updateWaterintakeById(waterintake);
        Map<String, Object> resultMap = new HashMap<String, Object>();
        resultMap.put("ifSucceed", true);
        return resultMap;
    }

    @RequestMapping("/delWaterintakeById")
    //根据取水口编号删除取水口信息
    public @ResponseBody Map<String, Object> delWaterIntakeById(Integer w_id) {
        System.out.println("取水口编号为：" + w_id);
        waterintakeService.delWaterIntakeById(w_id);

        Map<String, Object> resultParams = new HashMap<String, Object>();
        resultParams.put("ifSucceed", true);
        return resultParams;
    }
}

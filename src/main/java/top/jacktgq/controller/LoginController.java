package top.jacktgq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import top.jacktgq.pojo.UserInfo;
import top.jacktgq.service.LoginService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @Author zhourenxi
 * @Date 2020/4/13
 * @Description 用户登录前端控制器
 */
@Controller
public class LoginController {

    @Autowired
    LoginService loginService;

    //用户登录验证
    @RequestMapping(value = "/userLogin",method = RequestMethod.POST)
    public String login(UserInfo userInfo, Model model, HttpSession session){
        System.out.println("进来了");
        System.out.println(userInfo);

        UserInfo user = loginService.login(userInfo);
        System.out.println("user===="+user);

        if (user!=null){
            session.setAttribute("user", user);
            //判断用户角色
            if (user.getU_role()==1){
                System.out.println("管理员");
                return "redirect:admin/index.jsp";
            }else {
                System.out.println("普通用户");
                return "redirect:user/index.jsp";
            }
        }else {
            System.out.println("没有账号，请注册！");
            model.addAttribute("errorInfo", "用户名或密码错误");
            return "login";
        }
    }
}

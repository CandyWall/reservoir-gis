package top.jacktgq.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author zhou
 * @Date 2020/4/13
 * @Description 取水口实体类
 */
public class Waterintake {
    private Integer w_id;           //取水口名称
    private String geom;            //存储取水口空间坐标集
    private String w_name;          //取水口名称
    private String w_desc;          //取水口描述
    private String w_type;          //取水口类型
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date build_time;        //修建时间
    private String w_region;        //所属地区

    public Integer getW_id() {
        return w_id;
    }

    public void setW_id(Integer w_id) {
        this.w_id = w_id;
    }

    public String getGeom() {
        return geom;
    }

    public void setGeom(String geom) {
        this.geom = geom;
    }

    public String getW_name() {
        return w_name;
    }

    public void setW_name(String w_name) {
        this.w_name = w_name;
    }

    public String getW_desc() {
        return w_desc;
    }

    public void setW_desc(String w_desc) {
        this.w_desc = w_desc;
    }

    public String getW_type() {
        return w_type;
    }

    public void setW_type(String w_type) {
        this.w_type = w_type;
    }

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    public Date getBuild_time() {
        return build_time;
    }

    public void setBuild_time(Date build_time) {
        this.build_time = build_time;
    }

    public String getW_region() {
        return w_region;
    }

    public void setW_region(String w_region) {
        this.w_region = w_region;
    }

    @Override
    public String toString() {
        return "Waterintake{" +
                "w_id=" + w_id +
                ", geom='" + geom + '\'' +
                ", w_name='" + w_name + '\'' +
                ", w_desc='" + w_desc + '\'' +
                ", w_type='" + w_type + '\'' +
                ", build_time=" + build_time +
                ", w_region='" + w_region + '\'' +
                '}';
    }
}

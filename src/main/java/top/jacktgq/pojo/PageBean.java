package top.jacktgq.pojo;

import java.util.List;

/**
 *
 * @ClassName: PageBean
 * @Description: 封装分页信息的类
 * @author CandyWall
 * @param <E>
 *
 */
public class PageBean<E> {
	//当前页序号
	private int currentPage;
	//当前页显示条数
	private int currentCount;
	//总显示条数
	private int totalCount;
	//总显示页数
	private int totalPage;
	//实体具体实体类信息
	private List<E> list;

	public int getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	public int getCurrentCount() {
		return currentCount;
	}
	public void setCurrentCount(int currentCount) {
		this.currentCount = currentCount;
	}
	public int getTotalCount() {
		return totalCount;
	}
	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
	public int getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}
	public List<E> getList() {
		return list;
	}
	public void setList(List<E> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "PageBean{" +
				"currentPage=" + currentPage +
				", currentCount=" + currentCount +
				", totalCount=" + totalCount +
				", totalPage=" + totalPage +
				", list=" + list +
				'}';
	}
}

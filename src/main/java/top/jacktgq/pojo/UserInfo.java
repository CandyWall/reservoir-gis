package top.jacktgq.pojo;

/**
 * @Author zhou
 * @Date 2020/4/13
 * @Description 用户实体类
 */
public class UserInfo {
    private String u_id;  //编号
    private Integer u_role;  //用户角色
    private String u_password;  //用户密码
    private String u_name;  //用户姓名(账号)

    public String getU_id() {
        return u_id;
    }

    public void setU_id(String u_id) {
        this.u_id = u_id;
    }

    public Integer getU_role() {
        return u_role;
    }

    public void setU_role(Integer u_role) {
        this.u_role = u_role;
    }

    public String getU_password() {
        return u_password;
    }

    public void setU_password(String u_password) {
        this.u_password = u_password;
    }

    public String getU_name() {
        return u_name;
    }

    public void setU_name(String u_name) {
        this.u_name = u_name;
    }

    @Override
    public String toString() {
        return "UserInfo{" +
                "u_id='" + u_id + '\'' +
                ", u_role=" + u_role +
                ", u_password='" + u_password + '\'' +
                ", u_name='" + u_name + '\'' +
                '}';
    }
}

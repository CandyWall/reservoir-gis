package top.jacktgq.pojo;

import java.math.BigInteger;

/**
 * @Author zhourenxi
 * @Date 2020/4/13
 * @Description 保护区实体类
 */
public class Reserve {
    private Integer res_id;     //保护区编号
    private String geom;        //保护区空间坐标集
    private String res_name;    //保护区名称
    private String res_desc;    //保护区描述
    private Double res_area;    //保护区面积

    public Integer getRes_id() {
        return res_id;
    }

    public void setRes_id(Integer res_id) {
        this.res_id = res_id;
    }

    public String getGeom() {
        return geom;
    }

    public void setGeom(String geom) {
        this.geom = geom;
    }

    public String getRes_name() {
        return res_name;
    }

    public void setRes_name(String res_name) {
        this.res_name = res_name;
    }

    public String getRes_desc() {
        return res_desc;
    }

    public void setRes_desc(String res_desc) {
        this.res_desc = res_desc;
    }

    public Double getRes_area() {
        return res_area;
    }

    public void setRes_area(Double res_area) {
        this.res_area = res_area;
    }

    @Override
    public String toString() {
        return "Reserve{" +
                "res_id=" + res_id +
                ", geom='" + geom + '\'' +
                ", res_name='" + res_name + '\'' +
                ", res_desc='" + res_desc + '\'' +
                ", res_area=" + res_area +
                '}';
    }
}

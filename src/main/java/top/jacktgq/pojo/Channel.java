package top.jacktgq.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Author CandyWall
 * @Date 2020/4/12--17:06
 * @Description 渠道实体类
 */
public class Channel {
    private Integer c_id;       //渠道编号
    private String c_desc;      //渠道描述
    private Double c_length;    //渠道长度
    private String c_type;      //渠道类型
    private String c_level;     //渠道等级
    private String c_region;    //所属地区
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date build_time;    //修建时间
    private String geom;        //存储渠道空间坐标坐标

    public Integer getC_id() {
        return c_id;
    }

    public void setC_id(Integer c_id) {
        this.c_id = c_id;
    }

    public String getC_desc() {
        return c_desc;
    }

    public void setC_desc(String c_desc) {
        this.c_desc = c_desc;
    }

    public Double getC_length() {
        return c_length;
    }

    public void setC_length(Double c_length) {
        this.c_length = c_length;
    }

    public String getC_type() {
        return c_type;
    }

    public void setC_type(String c_type) {
        this.c_type = c_type;
    }

    public String getC_level() {
        return c_level;
    }

    public void setC_level(String c_level) {
        this.c_level = c_level;
    }

    public String getC_region() {
        return c_region;
    }

    public void setC_region(String c_region) {
        this.c_region = c_region;
    }

    @JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    public Date getBuild_time() {
        return build_time;
    }

    public void setBuild_time(Date build_time) {
        this.build_time = build_time;
    }

    public String getGeom() {
        return geom;
    }

    public void setGeom(String geom) {
        this.geom = geom;
    }

    @Override
    public String toString() {
        return "Channel{" +
                "c_id=" + c_id +
                ", c_desc='" + c_desc + '\'' +
                ", c_length=" + c_length +
                ", c_type='" + c_type + '\'' +
                ", c_level='" + c_level + '\'' +
                ", c_region='" + c_region + '\'' +
                ", build_time=" + build_time +
                ", geom='" + geom + '\'' +
                '}';
    }
}

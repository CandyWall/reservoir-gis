package top.jacktgq.service;

import top.jacktgq.pojo.PageBean;
import top.jacktgq.pojo.Waterintake;

import java.util.List;

/**
 * @Author zhou
 * @Date 2020/4/13
 * @Description 取水口信息的业务层接口
 */
public interface WaterintakeService {

    //查询用户信息并分页
    PageBean<Waterintake> getWaterIntakeInfos(Integer currentPage, Integer currentCount);

    //查询取水口的坐标集
    List<String> getWaterintakeGeom();

    //新增取水口
    void addWaterintake(Waterintake waterintake);

    //根据取水口编号查询取水口信息
    Waterintake getWaterintakeById(Integer w_id);

    //根据取水口编号，修改取水口信息
    void updateWaterintakeById(Waterintake waterintake);

    //根据取水口编号删除取水口信息
    void delWaterIntakeById(Integer w_id);
}

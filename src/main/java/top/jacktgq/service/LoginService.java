package top.jacktgq.service;

import top.jacktgq.pojo.UserInfo;
/**
 * @Author zhourenxi
 * @Date 2020/4/13
 * @Description 用户登录管理的业务层接口
 */
public interface LoginService {

    //用户登录验证
    UserInfo login(UserInfo userInfo);
}

package top.jacktgq.service;

import java.util.List;

/**
 * @Author CandyWall
 * @Date 2020/4/14--11:39
 * @Description 水库信息管理的业务层接口
 */
public interface ReservoirService {
    // 查询水库的坐标集
    List<String> getResevoirGeom();
}

package top.jacktgq.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.jacktgq.mapper.ChannelMapper;
import top.jacktgq.pojo.Channel;
import top.jacktgq.pojo.PageBean;
import top.jacktgq.pojo.Waterintake;
import top.jacktgq.service.ChannelService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author CandyWall
 * @Date 2020/4/12--16:38
 * @Description 渠道管理业务层实现类
 */
@Service
public class ChannelServiceImpl implements ChannelService {
    @Autowired
    ChannelMapper channelMapper;

    @Override
    public void addChannel(Channel channel) {
        channelMapper.addChannel(channel);
    }

    //查询渠道信息并分页
    @Override
    public PageBean<Channel> getChannelsByPage(Integer currentPage, Integer currentCount) {
        //新建分页对象
        PageBean<Channel> pageBean = new PageBean<Channel>();
        //1、封装当前页的页号
        pageBean.setCurrentPage(currentPage);

        //2、封装当前页显示的条数
        pageBean.setCurrentCount(currentCount);

        //3、封装显示的总条数
        int totalCount = channelMapper.getTotalCount();
        pageBean.setTotalCount(totalCount);

        //4、封装显示的总页数
        int totalPage = (int) Math.ceil(1.0 * totalCount / currentCount);
        pageBean.setTotalPage(totalPage);

        //5、封装当前页的信息
        //计算当前页第一条数据的索引
        int index = (currentPage - 1) * currentCount;
        Map<String,Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("index", index);
        paramsMap.put("currentCount", currentCount);
        List<Channel> channelInfoList = channelMapper.getChannelsByPage(paramsMap);
        pageBean.setList(channelInfoList);
        return pageBean;
    }

    @Override
    //查询渠道的坐标集
    public List<String> getChannelGeom() {
        return channelMapper.getChannelGeom();
    }

    @Override
    //统计各个渠道的长度
    public List<Map<String, Object>> lengthStatistics() {
        return channelMapper.lengthStatistics();
    }

    @Override
    public Channel getChannelById(Integer c_id) {
        return channelMapper.getChannelById(c_id);
    }

    @Override
    public void updateChannelById(Channel channel) {
        channelMapper.updateChannelById(channel);
    }

    @Override
    public void delChannelById(Integer c_id) {
        channelMapper.delChannelById(c_id);
    }
}

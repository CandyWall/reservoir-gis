package top.jacktgq.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.jacktgq.mapper.LoginMapper;
import top.jacktgq.pojo.UserInfo;
import top.jacktgq.service.LoginService;
/**
 * @Author zhourenxi
 * @Date 2020/4/13
 * @Description 用户登录管理业务层实现类
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    LoginMapper loginMapper;

    //用户登录验证
    @Override
    public UserInfo login(UserInfo userInfo) {
        return loginMapper.login(userInfo);
    }
}

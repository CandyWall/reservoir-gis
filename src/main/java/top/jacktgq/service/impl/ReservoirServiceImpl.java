package top.jacktgq.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.jacktgq.mapper.ReservoirMapper;
import top.jacktgq.service.ReservoirService;

import java.util.List;

/**
 * @Author CandyWall
 * @Date 2020/4/14--11:41
 * @Description 水库信息管理的业务层实现类
 */
@Service
public class ReservoirServiceImpl implements ReservoirService {
    @Autowired
    ReservoirMapper reservoirMapper;

    //查询水库的坐标集
    public List<String> getResevoirGeom() {
        return reservoirMapper.getResevoirGeom();
    }
}

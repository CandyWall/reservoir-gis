package top.jacktgq.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.jacktgq.mapper.WaterintakeMapper;
import top.jacktgq.pojo.PageBean;
import top.jacktgq.pojo.Waterintake;
import top.jacktgq.service.WaterintakeService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author zhou
 * @Date 2020/4/13
 * @Description 取水口信息管理业务层实现类
 */
@Service
public class WaterintakeServiceImpl implements WaterintakeService {

    @Autowired
    WaterintakeMapper waterintakeMapper;

    //查询用户信息并分页
    @Override
    public PageBean<Waterintake> getWaterIntakeInfos(Integer currentPage, Integer currentCount) {
        //新建分页对象
        PageBean<Waterintake> pageBean = new PageBean<Waterintake>();
        //1、封装当前页的页号
        pageBean.setCurrentPage(currentPage);

        //2、封装当前页显示的条数
        pageBean.setCurrentCount(currentCount);

        //3、封装显示的总条数
        int totalCount = waterintakeMapper.getTotalCount();
        pageBean.setTotalCount(totalCount);

        //4、封装显示的总页数
        int totalPage = (int) Math.ceil(1.0 * totalCount / currentCount);
        pageBean.setTotalPage(totalPage);

        //5、封装当前页的信息
        //计算当前页第一条数据的索引
        int index = (currentPage - 1) * currentCount;
        Map<String,Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("index", index);
        paramsMap.put("currentCount", currentCount);
        List<Waterintake> waterIntakeInfoList = waterintakeMapper.getWaterIntakeInfos(paramsMap);
        pageBean.setList(waterIntakeInfoList);
        return pageBean;
    }

    @Override
    //查询取水口的坐标集
    public List<String> getWaterintakeGeom() {
        return waterintakeMapper.getWaterintakeGeom();
    }

    @Override
    //新增取水口
    public void addWaterintake(Waterintake waterintake) {
        waterintakeMapper.addWaterintake(waterintake);
    }

    @Override
    //根据取水口编号查询取水口信息
    public Waterintake getWaterintakeById(Integer w_id) {
        return waterintakeMapper.getWaterintakeById(w_id);
    }

    @Override
    //根据取水口编号，修改取水口信息
    public void updateWaterintakeById(Waterintake waterintake) {
        waterintakeMapper.updateWaterintakeById(waterintake);
    }

    @Override
    //根据取水口编号删除取水口信息
    public void delWaterIntakeById(Integer w_id) {
        waterintakeMapper.delWaterIntakeById(w_id);
    }
}

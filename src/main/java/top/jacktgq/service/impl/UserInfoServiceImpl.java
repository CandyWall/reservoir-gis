package top.jacktgq.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.jacktgq.mapper.UserInfoMapper;
import top.jacktgq.pojo.PageBean;
import top.jacktgq.pojo.UserInfo;
import top.jacktgq.service.UserInfoService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author zhourenxi
 * @Date 2020/4/13
 * @Description 用户信息管理业务层实现类
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {
    @Autowired
    UserInfoMapper userInfoMapper;

    //查询用户信息并分页
    @Override
    public PageBean<UserInfo> getUserInfos(Integer currentPage, Integer currentCount) {
        //新建分页对象
        PageBean<UserInfo> pageBean = new PageBean<UserInfo>();
        //1、封装当前页的页号
        pageBean.setCurrentPage(currentPage);

        //2、封装当前页显示的条数
        pageBean.setCurrentCount(currentCount);

        //3、封装显示的总条数
        int totalCount = userInfoMapper.getTotalCount();
        pageBean.setTotalCount(totalCount);

        //4、封装显示的总页数
        int totalPage = (int) Math.ceil(1.0 * totalCount / currentCount);
        pageBean.setTotalPage(totalPage);

        //5、封装当前页的信息
        //计算当前页第一条数据的索引
        int index = (currentPage - 1) * currentCount;
        Map<String,Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("index", index);
        paramsMap.put("currentCount", currentCount);
        List<UserInfo> userInfoList = userInfoMapper.getUserInfos(paramsMap);
        pageBean.setList(userInfoList);
        return pageBean;
    }

    //添加用户
    @Override
    public void addUser(UserInfo userInfo) {
        userInfoMapper.addUser(userInfo);
    }

    //修改密码
    @Override
    public int updatePassword(String new_password, String user_id) {
        return userInfoMapper.updatePassword(new_password,user_id);
    }

    //根据用户编号查询用户信息
    @Override
    public UserInfo getUserById(String u_id) {
        return userInfoMapper.getUserById(u_id);
    }

    //根据用户编号，修改用户信息
    @Override
    public void updateUserById(UserInfo userInfo) {
        userInfoMapper.updateUserById(userInfo);
    }

    @Override
    public void delUserById(String u_id) {
        userInfoMapper.delUserById(u_id);
    }
}

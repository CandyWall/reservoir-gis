package top.jacktgq.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.jacktgq.mapper.ReserveMapper;
import top.jacktgq.pojo.PageBean;
import top.jacktgq.pojo.Reserve;
import top.jacktgq.service.ReserveService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author zhourenxi
 * @Date 2020/4/13
 * @Description 保护区信息管理业务层实现类
 */
@Service
public class ReserveServiceImpl implements ReserveService {

    @Autowired
    ReserveMapper reserveMapper;

    //查询保护区信息并分页
    @Override
    public PageBean<Reserve> getReserveInfos(Integer currentPage, Integer currentCount) {
        //新建分页对象
        PageBean<Reserve> pageBean = new PageBean<Reserve>();
        //1、封装当前页的页号
        pageBean.setCurrentPage(currentPage);

        //2、封装当前页显示的条数
        pageBean.setCurrentCount(currentCount);

        //3、封装显示的总条数
        int totalCount = reserveMapper.getTotalCount();
        pageBean.setTotalCount(totalCount);

        //4、封装显示的总页数
        int totalPage = (int) Math.ceil(1.0 * totalCount / currentCount);
        pageBean.setTotalPage(totalPage);

        //5、封装当前页的信息
        //计算当前页第一条数据的索引
        int index = (currentPage - 1) * currentCount;
        Map<String,Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("index", index);
        paramsMap.put("currentCount", currentCount);
        List<Reserve> reserveInfoList = reserveMapper.getReserveInfos(paramsMap);
        pageBean.setList(reserveInfoList);
        return pageBean;
    }

    @Override
    //查询保护区的坐标集
    public List<String> getReserveGeom() {
        return reserveMapper.getReserveGeom();
    }

    @Override
    //添加保护区
    public void addReserve(Reserve reserve) {
        reserveMapper.addReserve(reserve);
    }

    @Override
    //统计各个保护区的面积
    public List<Map<String, Object>> areaStatistics() {
        return reserveMapper.areaStatistics();
    }

    @Override
    public Reserve getReserveById(Integer res_id) {
        return reserveMapper.getReserveById(res_id);
    }

    @Override
    public void updateReserveById(Reserve reserve) {
        reserveMapper.updateReserveById(reserve);
    }

    @Override
    public void delReserveById(Integer res_id) {
        reserveMapper.delReserveById(res_id);
    }
}

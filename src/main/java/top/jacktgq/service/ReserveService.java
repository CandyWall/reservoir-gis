package top.jacktgq.service;

import top.jacktgq.pojo.Channel;
import top.jacktgq.pojo.PageBean;
import top.jacktgq.pojo.Reserve;

import java.util.List;
import java.util.Map;

/**
 * @Author zhou
 * @Date 2020/4/13
 * @Description 保护区信息的业务层接口
 */
public interface ReserveService {

    //查询保护区信息并分页
    PageBean<Reserve> getReserveInfos(Integer currentPage, Integer currentCount);

    //查询保护区坐标集
    List<String> getReserveGeom();

    //添加保护区
    void addReserve(Reserve reserve);

    //统计各个保护区的面积
    List<Map<String, Object>> areaStatistics();

    //根据保护区编号查询保护区信息
    Reserve getReserveById(Integer res_id);

    //根据保护区编号，修改保护区信息
    void updateReserveById(Reserve reserve);

    //根据保护区编号删除保护区信息
    void delReserveById(Integer res_id);
}

package top.jacktgq.service;

import top.jacktgq.pojo.PageBean;
import top.jacktgq.pojo.UserInfo;

/**
 * @Author zhou
 * @Date 2020/4/13
 * @Description 用户信息的业务层接口
 */
public interface UserInfoService {

    //查询用户信息并分页
    PageBean<UserInfo> getUserInfos(Integer currentPage, Integer currentCount);

    //添加用户
    void addUser(UserInfo userInfo);

    //修改密码
    int updatePassword(String new_password, String user_id);

    //根据用户编号查询用户信息
    UserInfo getUserById(String u_id);

    //根据用户编号，修改用户信息
    void updateUserById(UserInfo userInfo);

    //根据用户编号删除用户信息
    void delUserById(String u_id);
}

package top.jacktgq.service;

import top.jacktgq.pojo.Channel;
import top.jacktgq.pojo.PageBean;
import top.jacktgq.pojo.Waterintake;

import java.util.List;
import java.util.Map;

/**
 * @Author CandyWall
 * @Date 2020/4/12--16:37
 * @Description 渠道管理的业务层接口
 */
public interface ChannelService {
    void addChannel(Channel channel);

    //查询渠道信息并分页
    PageBean<Channel> getChannelsByPage(Integer currentPage, Integer currentCount);

    //查询渠道的坐标集
    List<String> getChannelGeom();

    //统计各个渠道的长度
    List<Map<String, Object>> lengthStatistics();

    //根据渠道编号查询渠道信息
    Channel getChannelById(Integer c_id);

    //根据渠道编号，修改渠道信息
    void updateChannelById(Channel channel);

    //根据渠道编号删除渠道信息
    void delChannelById(Integer c_id);
}

<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>滁州城西水库水源地保护规划管理平台</title>
    <link href="${pageContext.request.contextPath}/style/style.css" rel="stylesheet" type="text/css"/>
</head>

<body class="Login">
<!--Login-开始-->
<div class="Login_heand dk">
    <a href="#" class="Login_logo fl">
        <img src="images/logo.png" class="fl" />
        <p class="fl">滁州城西水库水源地保护规划管理平台</p>
        <div class="clear"></div>
    </a>
</div>
<div class="Login_k">
    <div class="Login_T">用户登录 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span style="color:red">${errorInfo}</span>
        <p></p>
    </div>
    <!--登录-开始-->
    <form class="Login_dl" action="${pageContext.request.contextPath}/userLogin.action" method="post" id="myform">
        <div class="Login_row">
            <input name="u_id" type="text" class="Login_input Login_inp1" placeholder="用户名" />
        </div>
        <div class="Login_row">
            <input name="u_password" type="password" class="Login_input Login_inp2" placeholder="密码" />
        </div>
        <label class="Login_pitch fl">
            <input type="radio" name="u_role" class=" fl" value="0" checked="checked"/><div class="fl">市民</div><div class="clear"></div>
            <input type="radio" name="u_role" class=" fl" value="1" /><div class="fl">管理员</div><div class="clear"></div>
        </label>
        <a href="javascript:;" class="Login_dla" onclick="document.getElementById('myform').submit();return false;">登录</a>
        <%--<div class="Login_wj fr">
            <a href="#">新用户注册</a>
        </div>--%>
        <div class="clear"></div>
    </form>
    <!--登录-结束-->
</div>
<!--Login-结束-->
</body>

</html>
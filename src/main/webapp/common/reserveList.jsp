<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>用表格列出所有的保护区信息</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/js/layui/css/layui.css" media="all">
    <style>
        .layui-table, .layui-table-view {
            margin: 0;
        }
    </style>
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/layui/layui.all.js" charset="utf-8"></script>
</head>
<body>

<table class="layui-hide" lay-filter="mytable" id="test"></table>


<!-- 注意：如果你直接复制所有代码到本地，上述js路径需要改成你本地的 -->
<script type="text/html" id="barDemo">
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script>
    layui.use('table', function () {
        var table = layui.table;

        table.render({
            toolbar: 'default'
            , defaultToolbar: ['filter', 'print', 'exports']
            , elem: '#test'
            , even: 'true'
            , url: '${pageContext.request.contextPath}/getReserveInfos.action'
            , cols: [[
                {field: 'res_id', width: 160, title: '保护区编号', sort: true}
                , {field: 'res_name', width: 180, title: '保护区名称'}
                , {field: 'res_desc', title: '保护区描述'}
                , {field: 'geom', title: '保护区路径的点坐标集'}
                , {field: 'res_area', width: 300, title: '保护区面积'}
                , {fixed: 'right', title: '操作', toolbar: '#barDemo', width: 150}
            ]]
            , page: true
            , limit: 20
            , limits: [5,10,15,20,25]
            , request: {
                pageName: 'currentPage'     //页码的参数名称，默认：page
                , limitName: 'currentCount' //每页数据量的参数名，默认：limit
            },
            response: {
                statusName: 'status'        //规定数据状态的字段名称，默认：code
                , statusCode: 200           //规定成功的状态码，默认：0
                , msgName: 'hint'           //规定状态信息的字段名称，默认：msg
            , countName: 'total'            //规定数据总数的字段名称，默认：count
                , dataName: 'rows'          //规定数据列表的字段名称，默认：data
            }
        });
        //监听头工具条
        table.on('toolbar(mytable)', function (obj) {
            var layEvent = obj.event;
            if(layEvent === "add") {
                location.href = "${pageContext.request.contextPath}/common/showReserve.jsp";
            }
        });
        //监听行内工具条
        table.on('tool(mytable)', function (obj) {  //注：tool是工具条事件名，mytable是table原始容器的属性 lay-filter="对应的值"
            var data = obj.data;                    //获得当前行数据
            var layEvent = obj.event;               //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var tr = obj.tr;                        //获得当前行 tr 的DOM对象
            if (layEvent === 'edit') { //查看
                //tr[0].cells[0].innerText可以获取表格某行的某个单元格的值
                layer.open({
                    type: 2,
                    title: ['修改保护区信息','font-size:18px'],
                    /*content: '${pageContext.request.contextPath}/common/updateReserve.jsp'/!* + tr[0].cells[0].innerText*!/*/
                    content: '${pageContext.request.contextPath}/showUpdateReserve.action?res_id=' + tr[0].cells[0].innerText, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
                    area: ['736px','630px']
                });
            } else if (layEvent === 'del') { //删除
                layer.confirm('真的删除该单位信息么', {icon: 2}, function (index) {
                    obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                    layer.close(index);
                    //向服务端发送删除指令
                    $.post(
                        "${pageContext.request.contextPath}/delReserveById.action",
                        {"res_id": tr[0].cells[0].innerText},
                        function (data) {
                            if (data.ifSucceed) {
                                location.reload();
                                parent.layer.msg('删除成功');
                            } else {
                                parent.layer.msg('删除失败');
                            }
                        },
                        "json"
                    );
                });
            }
        });
    });

</script>
</body>
</html>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>修改密码</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/js/layui/css/layui.css" media="all">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/layui/layui.all.js" charset="utf-8"></script>
    <link href="${pageContext.request.contextPath}/style/style.css" rel="stylesheet" type="text/css"/>
    <style>
        .newUser .layui-form-label {
            width: 115px;
        }
    </style>
</head>
<body>
<div style="padding: 40px; background-color: #F2F2F2;height: 100%">
    <div class="layui-card" style="width: 396px;">
        <div class="layui-card-header" style="font-weight:bold;font-size: 18px;padding: 0px 100px">修改密码
        </div>
        <div class="layui-card-body newUser">
            <form class="layui-form" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label">请输入原密码</label>
                    <div class="layui-input-inline">
                        <input type="text" name="old_password" lay-verify="required|name" placeholder="请输入原密码"
                               autocomplete="on"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">请输入新密码</label>
                    <div class="layui-input-inline">
                        <input type="text" id="new_password" name="new_password" lay-verify="required|name" placeholder="请重复输入新密码"
                               autocomplete="on"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">请重复输入新密码</label>
                    <div class="layui-input-inline">
                        <input type="text" name="repassword" lay-verify="required|password" placeholder="请重复输入新密码"
                               autocomplete="on"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                        <!-- <button type="reset" class="layui-btn layui-btn-primary">重置</button> -->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<script>
    //Demo
    layui.use('form', function () {
        var form = layui.form;
        var laydate = layui.laydate;
        //常规用法
        laydate.render({
            elem: '#build_time'
        });

        //监听提交
        form.on('submit(formDemo)', function (data) {
            //layer.msg(JSON.stringify(data.field));
            $.ajax({
                url:"${pageContext.request.contextPath}/updatePassword.action",
                async: false,
                type:"POST",
                dataType: "json",
                data:data.field,
                success: function(obj){
                    if (obj.ifSucceed == true) {
                        //parent.location.href = "${pageContext.request.contextPath}/login.jsp"
                        location.reload();
                        parent.layer.msg("修改成功！");
                    } else {
                        layer.msg("修改失败！");
                    }
                }
            });
            return false;
        });

        form.verify({
            password: function(value, item) {
                console.log(value,$("#new_password").val());
                if(value !== $("#new_password").val()) {
                    return "两次输入的密码不一致";
                }
            }
        });
    });
</script>
</body>
</html>

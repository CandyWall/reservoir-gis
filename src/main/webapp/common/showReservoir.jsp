<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>在地图上显示水库</title>
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/ol.css">
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        #layerControl {
        border-radius: 4px;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 20px;
        width: 190px;
        position: absolute;
        top: 20px;
        right: 20px;
        background: rgba(169, 169, 169,0.3);
        /* background: url(${pageContext.request.contextPath}/images/light.png) no-repeat;
        box-shadow: inset 0px 2px 118px #41a2ff; */
      }
    </style>
    <script src="${pageContext.request.contextPath}/js/ol.js"></script>
</head>
<body>
<div id="map" style="width: 100%"></div>
<div id="layerControl" class="layerControl">
    <ul id="layerTree" class="layerTree" style="list-style: none"></ul>
</div>
<script>
    var layer=new Array();//图层数组
    var layerName=new Array();//图层名称数组
    var layerVisibility=new Array();//图层可见数组
    function loadLayersControl(map,id){
        var treeContent=document.getElementById(id);
        var layers=map.getLayers();//获取地图中的所有图层
        for(var i=0;i<layers.getLength();i++){
            layer[i]=layers.item(i);
            layerName[i]=layer[i].get('name');
            layerVisibility[i]=layer[i].getVisible();//获取每个图层的名称及是否可见
            var elementLi=document.createElement("li");
            treeContent.appendChild(elementLi);
            var elementInput=document.createElement("input");
            elementInput.type="checkbox";
            elementInput.name="layers";
            elementLi.appendChild(elementInput);
            var elementLabel=document.createElement("label");
            elementLabel.className="layer";
            setInnerText(elementLabel,layerName[i]);
            elementLi.appendChild(elementLabel);
            //<ul><li><input type="checkbox" name="layers"/><label class="layer"></label></li></ul>
            if(layerVisibility[i]){
                elementInput.checked=true;
            }
            addChangeEvent(elementInput,layer[i]);
        }
    }
    function  addChangeEvent(element,layer){
        element.onclick=function(){
            if(element.checked){
                layer.setVisible(true);
            }else{
                layer.setVisible(false);
            }
        }
    }
    function setInnerText(element,text){
        if(typeof element.textContent=="string"){
            element.textContent=text;
        }else{
            element.innerText=text;//FireFox不支持innerText方法,兼容
        }
    }


    //////////////////////////////////////////////////////////////////////
    //读取水库信息
    $.get(
        "${pageContext.request.contextPath}/getReservoirGeom.action",
        function (data) {
            addGeoReservoirJson(data);
        },
        "json"
    );
    function addGeoReservoirJson(data) {
        var vectorSource = new ol.source.Vector({
            features: (new ol.format.GeoJSON()).readFeatures(data)
        });
        var surfaceLayer = new ol.layer.Vector({
            source: vectorSource,
            style: new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: '#ff0000',
                    width: 3
                }),
                radius: 10,
                fill: new ol.style.Fill({
                    color: '#ffff00',
                })
            }),
            name: '水库'
        });
        map.addLayer(surfaceLayer);
        loadLayersControl(map,"layerTree");
    }

    //创建地图
    var map = new ol.Map({
        layers: [
            /*new ol.layer.Tile({
                source: new ol.source.OSM()
            })*/
            //加载天地图数据源
            getTdtLayer("vec_w"),
            getTdtLayer("cva_w")
        ],
        view: new ol.View({
            center: [118.28,32.32],
            zoom: 14,
            projection: 'EPSG:4326'
        }),
        target: 'map'
    });
    //获取天地图图层
    function getTdtLayer(lyr) {
        var urls=[];
        for(var i=0;i<8;i++)
        {
            urls.push( "http://t"+i+".tianditu.com/DataServer?T=" + lyr + "&X={x}&Y={y}&L={z}&tk=a4c01359108c54c0c298e2e1c59c81c6")
        }
        var layer = new ol.layer.Tile({
            source: new ol.source.XYZ({
                urls:urls
                //url: "http://t4.tianditu.com/DataServer?T=vec_w&x={x}&y={y}&l={z}&tk=a4c01359108c54c0c298e2e1c59c81c6"
            }),
            name: '天地图底图'
        });
        return layer;
    };
    /*mapLayer = new ol.layer.Image({
        source : new ol.source.ImageWMS({
            url : 'http://localhost:9999/geoserver/ccgm_rhs/wms',
            params : {
                'LAYERS' : 'ccgm_rhs:chzu_south',
                'TILED' : true
            },
            ratio : 1,
            serverType : 'geoserver'
        }),
        name: '滁州学院栅格底图'
    });
    map.addLayer(mapLayer);*/

    //loadLayersControl(map,"layerTree");
</script>
</body>
</html>

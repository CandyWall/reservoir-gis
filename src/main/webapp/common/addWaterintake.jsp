<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>新增取水口信息</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/js/layui/css/layui.css" media="all">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/layui/layui.all.js" charset="utf-8"></script>
    <style>
        .newWaterintake .layui-form-label {
            width: 90px;
        }

        .newWaterintake .layui-input, .layui-textarea {
            width: 80%;
        }
    </style>
</head>
<body>
<div style="padding: 40px; background-color: #F2F2F2;">
    <div class="layui-card">
        <div class="layui-card-header" style="font-weight:bold;font-size: 18px;padding: 0px 100px">新增取水口信息</div>
        <div class="layui-card-body newWaterintake">
            <form class="layui-form" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label">取水口名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="w_name" lay-verify="required|name" placeholder="请输入取水口名称"
                               autocomplete="on"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">取水口描述</label>
                    <div class="layui-input-block">
                        <textarea name="w_desc" placeholder="请输入取水口描述" class="layui-textarea"></textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">取水口类型</label>
                    <div class="layui-input-block">
                        <input type="text" name="w_type" lay-verify="required" placeholder="请输入取水口类型"
                               autocomplete="on"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">取水口坐标</label>
                    <div class="layui-input-block">
                        <input id="w_pos" type="text" name="geom" lay-verify="required"
                               autocomplete="on" readonly
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">修建时间</label>
                    <div class="layui-input-block">
                      <input type="text" class="layui-input" id="build_time" name="build_time" placeholder="请输入修建时间">
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                        <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    //Demo
    layui.use('form', function () {
        var form = layui.form;
        var laydate = layui.laydate;
        //常规用法
        laydate.render({
            elem: '#build_time'
        });

        //监听提交
        form.on('submit(formDemo)', function (data) {
            //layer.msg(JSON.stringify(data.field));
            $.ajax({
                url:"${pageContext.request.contextPath}/addWaterintake.action",
                async: false,
                type:"POST",
                dataType: "json",
                data:data.field,
                success: function(obj){
                    if (obj.ifSucceed == true) {
                        parent.location.reload();
                        parent.parent.layer.msg("添加成功！");
                    } else {
                        layer.msg("添加失败！");
                    }
                }
            });
            return false;
        });

        // 1.先去掉？  substr('起始的位置'，截取几个字符);
        var params = location.search.substr(1); // geom=andy
        // console.log(params);
        // 2.利用&把字符串分割成数组
        var arr1 = params.split('&');
        // 2.利用=把字符串分割为数组 split('=');
        var arr2 = arr1[0].split('=');
        // 将坐标信息拼接成可以直接存到postgis数据库中的线条数据
        var sketchCoords = JSON.parse(arr2[1]);
        var geom_str = "POINT(" + sketchCoords[0] + " " + sketchCoords[1] + ")";
        $("#w_pos").val(geom_str);
    });
</script>
</body>
</html>

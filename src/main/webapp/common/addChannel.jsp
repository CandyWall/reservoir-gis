<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>新增渠道信息</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/js/layui/css/layui.css" media="all">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/layui/layui.all.js" charset="utf-8"></script>
    <style>
        .newChannel .layui-form-label {
            width: 100px;
        }

        .newChannel .layui-input, .layui-textarea {
            width: 80%;
        }
    </style>
</head>
<body>
<div style="padding: 40px; background-color: #F2F2F2;">
    <div class="layui-card">
        <div class="layui-card-header" style="font-weight:bold;font-size: 18px;padding: 0px 100px">新增渠道信息</div>
        <div class="layui-card-body newChannel">
            <form class="layui-form" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label">渠道名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="c_name" lay-verify="required" placeholder="请输入渠道名称"
                               autocomplete="on"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">渠道描述</label>
                    <div class="layui-input-block">
                        <textarea name="c_desc" placeholder="请输入渠道描述" lay-verify="required" class="layui-textarea"></textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">渠道类型</label>
                    <div class="layui-input-block">
                        <input type="text" name="c_type" lay-verify="required" placeholder="请输入渠道类型"
                               autocomplete="on"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">渠道等级</label>
                    <div class="layui-input-block">
                        <input type="text" name="c_level" lay-verify="required" placeholder="请输入渠道等级"
                               autocomplete="on"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">渠道路径点坐标</label>
                    <div class="layui-input-block">
                        <textarea id="c_geom" name="geom" class="layui-textarea" lay-verify="required" readonly></textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">渠道长度（米）</label>
                    <div class="layui-input-block">
                        <input id="c_length" type="text" name="c_length" lay-verify="required"
                               autocomplete="on" readonly
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">所属地区</label>
                    <div class="layui-input-block">
                        <input type="text" name="c_region" lay-verify="required" placeholder="请输入渠道所属地区"
                               autocomplete="on"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">修建时间</label>
                    <div class="layui-input-block">
                      <input type="text" name="build_time" class="layui-input" id="build_time" lay-verify="required" placeholder="请输入修建时间">
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                        <!-- <button type="reset" class="layui-btn layui-btn-primary">重置</button> -->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    //Demo
    layui.use('form', function () {
        var form = layui.form;
        var laydate = layui.laydate;
        //常规用法
        laydate.render({
            elem: '#build_time'
        });

        //监听提交
        form.on('submit(formDemo)', function (data) {
            //layer.msg(JSON.stringify(data.field));
            $.ajax({
                url:"${pageContext.request.contextPath}/addChannel.action",
                async: true,
                type:"POST",
                dataType: "json",
                data:data.field,
                success: function(obj){
                    if (obj.ifSucceed == true) {
                        parent.location.reload();
                        parent.parent.layer.msg("添加成功！");
                    } else {
                        layer.msg("添加失败！");
                    }
                }
            });
            return false;
        });

        // 1.先去掉？  substr('起始的位置'，截取几个字符);
        var params = location.search.substr(1); // geom=andy
        // console.log(params);
        // 2.利用&把字符串分割成数组
        var arr1 = params.split('&');
        // 2.利用=把字符串分割为数组 split('=');
        var arr2 = arr1[0].split('=');
        // 将坐标信息拼接成可以直接存到postgis数据库中的线条数据
        var sketchCoords = JSON.parse(arr2[1]);
        var coordinates = [];
        for(var i = 0; i < sketchCoords.length; i++) {
            coordinates.push(sketchCoords[i][0] + " " + sketchCoords[i][1]);
        }
        var geom_str = "MULTILINESTRING((" + coordinates.join(",") + "))";
        $("#c_geom").val(geom_str);
        var arr3 = arr1[1].split('=');
        $("#c_length").val(arr3[1]);
    });
</script>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <title>修改保护区信息</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/js/layui/css/layui.css" media="all">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/layui/layui.all.js" charset="utf-8"></script>
    <style>
        .newReserve .layui-form-label {
            width: 140px;
        }

        .newReserve .layui-input, .layui-textarea {
            width: 80%;
        }
    </style>
</head>
<body>
<div style="padding: 40px; background-color: #F2F2F2;">
    <div class="layui-card">
        <div class="layui-card-header" style="font-weight:bold;font-size: 18px;padding: 0px 100px">修改保护区信息</div>
        <div class="layui-card-body newReserve">
            <form class="layui-form" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label">保护区编号</label>
                    <div class="layui-input-block">
                        <input type="text" name="res_id" value="${reserve.res_id}" lay-verify="required" placeholder="请输入保护区编号"
                               autocomplete="on"
                               class="layui-input readonly">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">保护区名称</label>
                    <div class="layui-input-block">
                        <input type="text" name="res_name" value="${reserve.res_name}" lay-verify="required" placeholder="请输入保护区名称"
                               autocomplete="on"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">保护区描述</label>
                    <div class="layui-input-block">
                        <textarea name="res_desc" placeholder="请输入保护区描述" class="layui-textarea">${reserve.res_desc}</textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">保护区轮廓坐标集</label>
                    <div class="layui-input-block">
                        <textarea id="res_geom" name="geom" class="layui-textarea" readonly>${reserve.geom}</textarea>
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">保护区面积（平方米）</label>
                    <div class="layui-input-block">
                        <input id="res_area" type="text" value="${reserve.res_area}" name="res_area" lay-verify="required"
                               autocomplete="on" readonly
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                        <!-- <button type="reset" class="layui-btn layui-btn-primary">重置</button> -->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    //Demo
    layui.use('form', function () {
        var form = layui.form;
        var laydate = layui.laydate;
        //常规用法
        laydate.render({
            elem: '#build_time'
        });

        //监听提交
        form.on('submit(formDemo)', function (data) {
            //layer.msg(JSON.stringify(data.field));
            $.ajax({
                url:"${pageContext.request.contextPath}/updateReserveById.action",
                async: false,
                type:"POST",
                dataType: "json",
                data:data.field,
                success: function(obj){
                    if (obj.ifSucceed == true) {
                        parent.location.reload();
                        parent.parent.layer.msg("添加成功！");
                    } else {
                        layer.msg("添加失败！");
                    }
                }
            });
            return false;
        });
    });
</script>
</body>
</html>

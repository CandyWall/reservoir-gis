<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html>
  <head>
    <title>在地图上显示所有的保护区</title>

    <style>
      #layerControl {
        border-radius: 4px;
        padding: 10px;
        width: 190px;
        position: absolute;
        top: 20px;
        right: 20px;
        background: rgba(169, 169, 169,0.3);
      }
      #box {
        border-radius: 4px;
        padding: 10px;
        text-align: center;
        width: 190px;
        position: absolute;
        top: 200px;
        right: 20px;
        background: rgba(169, 169, 169,0.3);
      }
      #layerControl {
        border-radius: 4px;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 20px;
        width: 190px;
        position: absolute;
        top: 20px;
        right: 20px;
        background: rgba(169, 169, 169,0.3);
        /* background: url(${pageContext.request.contextPath}/images/light.png) no-repeat;
        box-shadow: inset 0px 2px 118px #41a2ff; */
      }
    </style>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/js/layui/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/ol.css" />
  </head>
  <body>
    <div id="map" style="width: 100%"></div>
    <div
      id="layerControl"
      class="layerControl"
      style="width:190px;position:absolute;top:20px;right:20px;background: rgba(169, 169, 169,0.3)">
      <ul id="layerTree" class="layerTree" style="list-style: none"></ul>
    </div>
    <div id="box">
      <form class="form-inline">
        <input id="enableAdd" type="checkbox" value="" /><label>&nbsp;点击开启添加保护区</label><br><br>
        <label>绘制类型 &nbsp;</label>
        <select id="polygonType">
          <option value="regular">绘制规则图形</option>
          <option value="irregular">绘制不规则图形</option>
        </select>
      </form>
    </div>
    <script src="${pageContext.request.contextPath}/js/ol.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/layui/layui.all.js"></script>
    <script>

      $(function() {
            var layer = new Array(); //图层数组
            var layerName = new Array(); //图层名称数组
            var layerVisibility = new Array(); //图层可见数组
            function loadLayersControl(map, id) {
                var treeContent = document.getElementById(id);
                var layers = map.getLayers(); //获取地图中的所有图层
                for (var i = 0; i < layers.getLength(); i++) {
                    layer[i] = layers.item(i);
                    layerName[i] = layer[i].get("name");
                    layerVisibility[i] = layer[i].getVisible(); //获取每个图层的名称及是否可见
                    var elementLi = document.createElement("li");
                    treeContent.appendChild(elementLi);
                    var elementInput = document.createElement("input");
                    elementInput.type = "checkbox";
                    elementInput.name = "layers";
                    elementLi.appendChild(elementInput);
                    var elementLabel = document.createElement("label");
                    elementLabel.className = "layer";
                    setInnerText(elementLabel, layerName[i]);
                    elementLi.appendChild(elementLabel);
                    //<ul><li><input type="checkbox" name="layers"/><label class="layer"></label></li></ul>
                    if (layerVisibility[i]) {
                        elementInput.checked = true;
                    }
                    addChangeEvent(elementInput, layer[i]);
                }
            }
            function addChangeEvent(element, layer) {
                element.onclick = function() {
                if (element.checked) {
                    layer.setVisible(true);
                } else {
                    layer.setVisible(false);
                }
                };
            }
            function setInnerText(element, text) {
                if (typeof element.textContent == "string") {
                element.textContent = text;
                } else {
                element.innerText = text; //FireFox不支持innerText方法,兼容
                }
            }

            //////////////////////////////////////////////////////////////////////
            //读取保护区信息
            $.post(
                    "${pageContext.request.contextPath}/getReserveGeom.action",
                    function (data) {
                      addReserveJson(data);
                    },
                    "json"
            );
            function addReserveJson(data) {
              var vectorSource = new ol.source.Vector({
                features: (new ol.format.GeoJSON()).readFeatures(data)
              });
              var reserveLayer = new ol.layer.Vector({
                source: vectorSource,
                style: new ol.style.Style({
                  stroke: new ol.style.Stroke({
                    color: '#0000ff',
                    width: 3
                  }),
                  radius: 10,
                  fill: new ol.style.Fill({
                    color: "#ffff00"
                  })
                }),
                name: '渠道'
              });
              map.addLayer(reserveLayer);
              loadLayersControl(map,"layerTree");
            }


            var source = new ol.source.Vector({
			    wrapX: false
            });

            var vector = new ol.layer.Vector({
                source: source,
                style: new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        color: '#0000ff',
                        width: 3
                    }),
                    radius: 10,
                    fill: new ol.style.Fill({
                        color: "#ffff00"
                    })
                }),
                name: "新增保护区"
            });

            //创建地图
            var map = new ol.Map({
                layers: [
                    /*new ol.layer.Tile({
                            source: new ol.source.OSM()
                        })*/
                    //加载天地图数据源
                    getTdtLayer("vec_w"),
                    getTdtLayer("cva_w"),
                    vector
                ],
                view: new ol.View({
                center: [118.28, 32.32],
                zoom: 14,
                projection: "EPSG:4326"
                }),
                target: "map"
            });
            //获取天地图图层
            function getTdtLayer(lyr) {
                var urls = [];
                for (var i = 0; i < 8; i++) {
                urls.push(
                    "http://t" +
                    i +
                    ".tianditu.com/DataServer?T=" +
                    lyr +
                    "&X={x}&Y={y}&L={z}&tk=a4c01359108c54c0c298e2e1c59c81c6"
                );
                }
                var layer = new ol.layer.Tile({
                source: new ol.source.XYZ({
                    urls: urls
                    //url: "http://t4.tianditu.com/DataServer?T=vec_w&x={x}&y={y}&l={z}&tk=a4c01359108c54c0c298e2e1c59c81c6"
                }),
                name: "天地图底图"
                });
                return layer;
            }

            var draw;

            $("#polygonType").change(function() {
                $("#enableAdd").change();
            });

            // 给点击开启添加保护区复选框加上事件监听
            $("#enableAdd").change(function() {
                var freehand = $("#polygonType").val() === "irregular" ? true : false;
                // console.log(freehand);
                if(draw) {
                    map.removeInteraction(draw);
                }
                //开启添加功能
                if(this.checked) {
                    draw = new ol.interaction.Draw({
                        source: source,
                        type: "Polygon",
                        style: new ol.style.Style({
                            stroke: new ol.style.Stroke({
                                color: "#ffcc33",
                                width: 2
                            }),
                            image: new ol.style.Circle({
                                radius: 7,
                                fill: new ol.style.Fill({
                                    color: "#ffcc33"
                                })
                            })
                        }),
                        freehand: freehand
                    });
                    //绘制结束事件
                    draw.on(
                        "drawend",
                        function(e) {
                            // console.log(e.target.sketchCoords_);
                            //获取初始坐标系
                            var sourceProj = map.getView().getProjection();
                            //克隆该几何对象然后转换坐标系
                            var geom = e.feature.getGeometry().clone().transform(sourceProj, "EPSG:3857");
                            //计算面积
                            var area = ol.sphere.getArea(geom);
                            // console.log(length);
                            layui.layer.open({
                                type: 2,
                                title: ['新增保护区信息','font-size:18px'],
                                content: "${pageContext.request.contextPath}/common/addReserve.jsp?geom=" + JSON.stringify(e.target.sketchCoords_) + "&area=" + area, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
                                area: ['659px','575px']
                            });
                        },
                        this
                    );
                    map.addInteraction(draw);
                } else {
                    map.removeInteraction(draw);
                }
            });
        });
    </script>
  </body>
</html>
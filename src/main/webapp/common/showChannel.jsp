<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html>
  <head>
    <title>在地图上显示所有的渠道</title>

    <style>
      #layerControl {
        border-radius: 4px;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 20px;
        width: 190px;
        position: absolute;
        top: 20px;
        right: 20px;
        background: rgba(169, 169, 169,0.3);
        /* background: url(${pageContext.request.contextPath}/images/light.png) no-repeat;
        box-shadow: inset 0px 2px 118px #41a2ff; */
      }
      #box {
        border-radius: 4px;
        padding: 10px;
        text-align: center;
        width: 190px;
        position: absolute;
        top: 200px;
        right: 20px;
        background: rgba(169, 169, 169,0.3);
      }
    </style>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/js/layui/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/style/ol.css" />
  </head>
  <body>
    <div id="map" style="width: 100%"></div>
    <div
      id="layerControl"
      class="layerControl"
      style="width:190px;position:absolute;top:20px;right:20px;background: rgba(169, 169, 169,0.3)">
      <ul id="layerTree" class="layerTree" style="list-style: none"></ul>
    </div>
    <div id="box">
      <form class="form-inline">
        <input id="enableAdd" type="checkbox" value="" /><label>&nbsp;点击开启添加渠道</label><br><br>
        <label>绘制类型 &nbsp;</label>
        <select id="lineType">
          <option value="straightLine">绘制直线</option>
          <option value="curve">绘制曲线</option>
        </select>
      </form>
    </div>
    <script src="${pageContext.request.contextPath}/js/ol.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/layui/layui.all.js"></script>
    <script>

      $(function() {
            var layer = new Array(); //图层数组
            var layerName = new Array(); //图层名称数组
            var layerVisibility = new Array(); //图层可见数组
            function loadLayersControl(map, id) {
                var treeContent = document.getElementById(id);
                var layers = map.getLayers(); //获取地图中的所有图层
                for (var i = 0; i < layers.getLength(); i++) {
                    layer[i] = layers.item(i);
                    layerName[i] = layer[i].get("name");
                    layerVisibility[i] = layer[i].getVisible(); //获取每个图层的名称及是否可见
                    var elementLi = document.createElement("li");
                    treeContent.appendChild(elementLi);
                    var elementInput = document.createElement("input");
                    elementInput.type = "checkbox";
                    elementInput.name = "layers";
                    elementLi.appendChild(elementInput);
                    var elementLabel = document.createElement("label");
                    elementLabel.className = "layer";
                    setInnerText(elementLabel, layerName[i]);
                    elementLi.appendChild(elementLabel);
                    //<ul><li><input type="checkbox" name="layers"/><label class="layer"></label></li></ul>
                    if (layerVisibility[i]) {
                        elementInput.checked = true;
                    }
                    addChangeEvent(elementInput, layer[i]);
                }
            }
            function addChangeEvent(element, layer) {
                element.onclick = function() {
                if (element.checked) {
                    layer.setVisible(true);
                } else {
                    layer.setVisible(false);
                }
                };
            }
            function setInnerText(element, text) {
                if (typeof element.textContent == "string") {
                element.textContent = text;
                } else {
                element.innerText = text; //FireFox不支持innerText方法,兼容
                }
            }

            //////////////////////////////////////////////////////////////////////
            //读取渠道信息
            $.get(
                "${pageContext.request.contextPath}/getChannelGeom.action",
                function (data) {
                    addChannelJson(data);
                },
                "json"
            );
            function addChannelJson(data) {
                var vectorSource = new ol.source.Vector({
                    features: (new ol.format.GeoJSON()).readFeatures(data)
                });
                var channelLayer = new ol.layer.Vector({
                    source: vectorSource,
                    style: new ol.style.Style({
                        stroke: new ol.style.Stroke({
                            color: '#0000ff',
                            width: 3
                        })
                    }),
                    name: '渠道'
                });
                map.addLayer(channelLayer);
                loadLayersControl(map,"layerTree");
            }

            var source = new ol.source.Vector({
			    wrapX: false
            });

            var vector = new ol.layer.Vector({
                source: source,
                style: new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        color: '#0000ff',
                        width: 3
                    })
                }),
                name: "新增渠道"
            });

            //创建地图
            var map = new ol.Map({
                layers: [
                    /*new ol.layer.Tile({
                            source: new ol.source.OSM()
                        })*/
                    //加载天地图数据源
                    getTdtLayer("vec_w"),
                    getTdtLayer("cva_w"),
                    vector
                ],
                view: new ol.View({
                center: [118.28, 32.32],
                zoom: 14,
                projection: "EPSG:4326"
                }),
                target: "map"
            });
            //获取天地图图层
            function getTdtLayer(lyr) {
                var urls = [];
                for (var i = 0; i < 8; i++) {
                urls.push(
                    "http://t" +
                    i +
                    ".tianditu.com/DataServer?T=" +
                    lyr +
                    "&X={x}&Y={y}&L={z}&tk=a4c01359108c54c0c298e2e1c59c81c6"
                );
                }
                var layer = new ol.layer.Tile({
                source: new ol.source.XYZ({
                    urls: urls
                    //url: "http://t4.tianditu.com/DataServer?T=vec_w&x={x}&y={y}&l={z}&tk=a4c01359108c54c0c298e2e1c59c81c6"
                }),
                name: "天地图底图"
                });
                return layer;
            }

            var draw;

            $("#lineType").change(function() {
                $("#enableAdd").change();
            });

            // 给点击开启添加取水口复选框加上事件监听
            $("#enableAdd").change(function() {
                var freehand = $("#lineType").val() === "curve" ? true : false;
                console.log(freehand);
                if(draw) {
                    map.removeInteraction(draw);
                }
                //开启添加功能
                if(this.checked) {
                    draw = new ol.interaction.Draw({
                        source: source,
                        type: "LineString",
                        style: new ol.style.Style({
                            stroke: new ol.style.Stroke({
                                color: "#ffcc33",
                                width: 2
                            }),
                            image: new ol.style.Circle({
                                radius: 7,
                                fill: new ol.style.Fill({
                                color: "#ffcc33"
                                })
                            })
                        }),
                        freehand: freehand
                    });
                    //绘制结束事件
                    draw.on(
                        "drawend",
                        function(e) {
                            // console.log(e.target.sketchCoords_);
                            //获取初始坐标系
                            var sourceProj = map.getView().getProjection();
                            //克隆该几何对象然后转换坐标系
                            var geom = e.feature.getGeometry().clone().transform(sourceProj, "EPSG:3857");
                            //计算长度
                            var length = ol.sphere.getLength(geom);
                            // console.log(length);

                            layui.layer.open({
                                type: 2,
                                title: ['新增渠道信息','font-size:18px'],
                                content: "${pageContext.request.contextPath}/common/addChannel.jsp?geom=" + JSON.stringify(e.target.sketchCoords_) + "&length=" + length, //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
                                area: ['643px','787px']
                            });
                        },
                        this
                    );
                    map.addInteraction(draw);
                } else {
                    map.removeInteraction(draw);
                }
            });
        });

      
    </script>
  </body>
</html>
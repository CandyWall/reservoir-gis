<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html style="height: 100%">

	<head>
		<meta charset="utf-8">
		<title>水库数据统计</title>
	</head>
	<body style="height: 100%; margin: 0">
		<div id="container" style="height: 100%;width: 100%"></div>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/js/echarts.min.js" ></script>
		<script type="text/javascript">
			$.post(
				"${pageContext.request.contextPath}/areaStatistics.action",	//统计各个保护区的面积
				function(res_id_areas) {
					//编号
					var res_ids = [];
					var res_areas = [];
					for(var i = 0; i < res_id_areas.length; i++) {
						res_ids.push(res_id_areas[i].res_id);
						res_areas.push(res_id_areas[i].res_area);
					}
					var c_ids = [];
					var c_lengths = [];
					$.post(
						"${pageContext.request.contextPath}/lengthStatistics.action",	//统计各个渠道的长度
						function(c_id_lengths) {
							for(var i = 0; i < c_id_lengths.length; i++) {
								c_ids.push(c_id_lengths[i].c_id);
								c_lengths.push(c_id_lengths[i].c_length);
							}
							// console.log("res_ids:" + res_ids);
							// console.log("res_areas:" + res_areas);
							// console.log("c_ids:" + c_ids);
							// console.log("c_lengths:" + c_lengths);
							loadCharts(res_ids, res_areas, c_ids, c_lengths);
						},
						"json"
					)
				},
				"json"
			);

			//加载图表
			function loadCharts(res_ids, res_areas, c_ids, c_lengths) {
				var dom = document.getElementById("container");
				var myChart = echarts.init(dom);
				var app = {};
				option = null;
				option = {
					grid: [
						{top:'20%',  left: '10%', width: '35%', height: '60%'},
						{top: "20%", right: '10%', width: '35%', height: '20%'},
						{bottom: "20%", right: '10%', width: '35%', height: '20%'}
					],
					title: {
						show: true,
						text: "城西水库2019年水位变化、保护区面积和渠道长度统计",
						// textAlign: "left"
						left: 'center',
						top: '5%'
					},
					legend: {
						bottom:"5%",
					},
					xAxis: [{
						show: true,
						type: 'category',	//type的类型可以是value，数值类型
						gridIndex: 0,
						//type: 'value',min: 0,max:7,
						//type: 'value',min:0,max:100,
						//splitNumber: 5,	//将坐标轴分五段
						//inverse: true,		//坐标轴反转
						boundaryGap: true,	//坐标轴刻度
						data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', "9月", "10月", "11月", "12月"],
						position: 'bottom', //默认为bottom，可以设置为top
						name: '月份',
						nameLocation: 'end',	//坐标轴的位置，可以设置为start,middle|center，end
						nameGap: '30',		//坐标轴名称与坐标轴之间的空隙
						// nameRotate: '45', 	//坐标轴名称旋转
						// offset: -30,		//负值为x轴向下偏移
						axisLine: {			//坐标轴轴线的相关设置
							show: true,
							lineStyle: {
								color: '#f00',
								width: 1,
								type: 'solid',		//坐标轴轴线线的类型：可以设置为solid，dashed，dotted
								shadowColor: 'rgba(0, 0, 0, 0.5)',
								shadowBlur: 10,
								shadowOffsetX: 5,
								shadowOffsetY: 5,
								opacity:0.5,
							},
							symbol: ['none', 'arrow'],
							symbolOffset: [0,10],
						},
						axisTick: {				//设置刻度
							show: true,
							length: 10,
							inside: true,
						},
						axisLabel: {
							show: true,
							fontSize: 20,
							color: 'black',
						},
						splitLine: {
							show: true,
						},
						splitArea: {
							show: true,
						}
					},
						{
							show: true,
							type: 'category',	//type的类型可以是value，数值类型
							gridIndex: 1,
							//type: 'value',min: 0,max:7,
							//type: 'value',min:0,max:100,
							//splitNumber: 5,	//将坐标轴分五段
							//inverse: true,		//坐标轴反转
							boundaryGap: true,	//坐标轴刻度
							data: res_ids,
							position: 'bottom', //默认为bottom，可以设置为top
							name: '保护区编号',
							nameLocation: 'end',	//坐标轴的位置，可以设置为start,middle|center，end
							nameGap: '30',		//坐标轴名称与坐标轴之间的空隙
							// nameRotate: '45', 	//坐标轴名称旋转
							// offset: -30,		//负值为x轴向下偏移
							axisLine: {			//坐标轴轴线的相关设置
								show: true,
								lineStyle: {
									color: '#f00',
									width: 1,
									type: 'solid',		//坐标轴轴线线的类型：可以设置为solid，dashed，dotted
									shadowColor: 'rgba(0, 0, 0, 0.5)',
									shadowBlur: 10,
									shadowOffsetX: 5,
									shadowOffsetY: 5,
									opacity:0.5,
								},
								symbol: ['none', 'arrow'],
								symbolOffset: [0,10],
							},
							axisTick: {				//设置刻度
								show: true,
								length: 10,
								inside: true,
							},
							axisLabel: {
								show: true,
								fontSize: 20,
								color: 'black',
							},
							splitLine: {
								show: true,
							},
							splitArea: {
								show: true,
							}
						},{
							show: true,
							type: 'category',	//type的类型可以是value，数值类型
							gridIndex: 2,
							//type: 'value',min: 0,max:7,
							//type: 'value',min:0,max:100,
							//splitNumber: 5,	//将坐标轴分五段
							//inverse: true,		//坐标轴反转
							boundaryGap: true,	//坐标轴刻度
							data: c_ids,
							position: 'bottom', //默认为bottom，可以设置为top
							name: '渠道编号',
							nameLocation: 'end',	//坐标轴的位置，可以设置为start,middle|center，end
							nameGap: '30',		//坐标轴名称与坐标轴之间的空隙
							// nameRotate: '45', 	//坐标轴名称旋转
							// offset: -30,		//负值为x轴向下偏移
							axisLine: {			//坐标轴轴线的相关设置
								show: true,
								lineStyle: {
									color: '#f00',
									width: 1,
									type: 'solid',		//坐标轴轴线线的类型：可以设置为solid，dashed，dotted
									shadowColor: 'rgba(0, 0, 0, 0.5)',
									shadowBlur: 10,
									shadowOffsetX: 5,
									shadowOffsetY: 5,
									opacity:0.5,
								},
								symbol: ['none', 'arrow'],
								symbolOffset: [0,10],
							},
							axisTick: {				//设置刻度
								show: true,
								length: 10,
								inside: true,
							},
							axisLabel: {
								show: true,
								fontSize: 20,
								color: 'black',
							},
							splitLine: {
								show: true,
							},
							splitArea: {
								show: true,
							}
						}],
					yAxis: [{
						type: 'value',
						gridIndex: 0,
						boundaryGap: true,	//坐标轴刻度
						name: '水位（米）',
						nameLocation: 'end',	//坐标轴的位置，可以设置为start,middle|center，end
						nameGap: '30',
						min: 0,
						max: 40,
						axisLine: {			//坐标轴轴线的相关设置
							show: true,
							lineStyle: {
								color: 'black',
								width: 1,
								type: 'solid',		//坐标轴轴线线的类型：可以设置为solid，dashed，dotted
								shadowColor: 'rgba(0, 0, 0, 0.5)',
								shadowBlur: 10,
								shadowOffsetX: 5,
								shadowOffsetY: 5,
								opacity:0.5,
							},
							symbol: ['none', 'arrow'],
							symbolOffset: [0,10],
						},
						// minorSplitLine: {
						// 	show: true,
						// 	lineStyle: {
						// 		color: '#ddd'
						// 	}
						// },
					},
						{
							type: 'value',
							gridIndex: 1,
							boundaryGap: true,	//坐标轴刻度
							name: '面积（平方米）',
							nameLocation: 'end',	//坐标轴的位置，可以设置为start,middle|center，end
							nameGap: '30',
							// min: 0,
							// max: 40,
							axisLine: {			//坐标轴轴线的相关设置
								show: true,
								lineStyle: {
									color: 'black',
									width: 1,
									type: 'solid',		//坐标轴轴线线的类型：可以设置为solid，dashed，dotted
									shadowColor: 'rgba(0, 0, 0, 0.5)',
									shadowBlur: 10,
									shadowOffsetX: 5,
									shadowOffsetY: 5,
									opacity:0.5,
								},
								symbol: ['none', 'arrow'],
								symbolOffset: [0,10],
							},
							// minorSplitLine: {
							// 	show: true,
							// 	lineStyle: {
							// 		color: '#ddd'
							// 	}
							// },
						},
						{
							type: 'value',
							gridIndex: 2,
							boundaryGap: true,	//坐标轴刻度
							name: '长度（米）',
							nameLocation: 'end',	//坐标轴的位置，可以设置为start,middle|center，end
							nameGap: '30',
							// min: 0,
							// max: 40,
							axisLine: {			//坐标轴轴线的相关设置
								show: true,
								lineStyle: {
									color: 'black',
									width: 1,
									type: 'solid',		//坐标轴轴线线的类型：可以设置为solid，dashed，dotted
									shadowColor: 'rgba(0, 0, 0, 0.5)',
									shadowBlur: 10,
									shadowOffsetX: 5,
									shadowOffsetY: 5,
									opacity:0.5,
								},
								symbol: ['none', 'arrow'],
								symbolOffset: [0,10],
							},
							// minorSplitLine: {
							// 	show: true,
							// 	lineStyle: {
							// 		color: '#ddd'
							// 	}
							// },
						}],
					dataZoom: [ {
						show: true,
						type: 'inside',
						filterMode: 'none',
						yAxisIndex: 0,
						startValue: 22,
						endValue: 30
					}],
					series: [{
						data: [28.76,28.93,29.14,28.86,28.21,27.53,27.01,26.40,25.37,24.65,23.83,23.94],
						//data: [[0,820],[1,932],[2,901],[3,934],[4,1290],[5,1330],[6,1320]],
						//如果type为value时，必须为二位数组的形式赋值
						//data: [820, 932, 901, 934, 1290],
						type: 'line',
						//areaStyle: {}	//是否填充折线以下的面
						name: '2019年度不同月份城西水库水位线变化',
						// step: true,
						lineStyle: {
							color: '#f00',
							width: 2,
							type: 'solid',		//坐标轴轴线线的类型：可以设置为solid，dashed，dotted
							shadowColor: 'rgba(0, 0, 0, 0.5)',
							shadowBlur: 10,
							shadowOffsetX: 5,
							shadowOffsetY: 5,
							opacity:0.5,
						},
						itemStyle: {
							color: '#f00',
							borderColor: '#000',
							borderwidth: 2,
							shadowColor: 'rgba(0, 0, 0, 0.5)',
							shadowBlur: 10,
							shadowOffsetX: 5,
							shadowOffsetY: 5,
							opacity:0.5,
						},
						emphasis: {
							label: {
								show: true
							},
							itemStyle: {
								color: '#f00',
								borderColor: '#ccc',
								borderwidth: 5,
								shadowColor: 'rgba(0, 0, 0, 0.5)',
								shadowBlur: 10,
								shadowOffsetX: 5,
								shadowOffsetY: 5,
								opacity:0.5,
							}
						},
						// smooth: true, //曲线是否平滑
						markPoint: {
							data: [ {
								name: '最大值',
								type: 'max',
								itemStyle:{color:'#0f0'},
								label: {
									show: true
								},
								emphasis: {
									itemStyle:{color:'#fc0'},
									label: {
										show: true
									}
								}
							}, {
								name: '最小值',
								type: 'min'
							},
								// {
								//     name: '均值',
								//     type: 'average'
								// },
							]
						},
					},
						{
							data: res_areas,
							//data: [[0,820],[1,932],[2,901],[3,934],[4,1290],[5,1330],[6,1320]],
							//如果type为value时，必须为二位数组的形式赋值
							//data: [820, 932, 901, 934, 1290],
							type: 'bar',
							//areaStyle: {}	//是否填充折线以下的面
							name: '各个保护区面积柱状图',
							// stack: {},
							emphasis: {
								label: {
									show: true
								},
								itemStyle: {
									color: '#f00',
									borderColor: '#ccc',
									borderwidth: 5,
									shadowColor: 'rgba(0, 0, 0, 0.5)',
									shadowBlur: 10,
									shadowOffsetX: 5,
									shadowOffsetY: 5,
									opacity:0.5,
								}
							},
							xAxisIndex: 1,
							yAxisIndex: 1
						},
						{
							data: c_lengths,
							//data: [[0,820],[1,932],[2,901],[3,934],[4,1290],[5,1330],[6,1320]],
							//如果type为value时，必须为二位数组的形式赋值
							//data: [820, 932, 901, 934, 1290],
							type: 'bar',
							//areaStyle: {}	//是否填充折线以下的面
							name: '各个渠道长度柱状图',
							// stack: {},
							emphasis: {
								label: {
									show: true
								},
								itemStyle: {
									color: '#f00',
									borderColor: '#ccc',
									borderwidth: 5,
									shadowColor: 'rgba(0, 0, 0, 0.5)',
									shadowBlur: 10,
									shadowOffsetX: 5,
									shadowOffsetY: 5,
									opacity:0.5,
								}
							},
							xAxisIndex: 2,
							yAxisIndex: 2
						}
					]
				};
				if(option && typeof option === "object") {
					myChart.setOption(option, true);
				}
			}
		</script>
	</body>

</html>
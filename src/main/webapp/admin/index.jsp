<%--
  Created by IntelliJ IDEA.
  User: zhourenxi
  Date: 2020/4/13
  Time: 14:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>滁州城西水库水源地保护规划管理平台-后台管理系统</title>
    <!-- <link rel="stylesheet" href="../js/layui/css/layui.css" media="all"> -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/font.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/xadmin.css">
</head>
<body>
<!-- 顶部开始 -->
<div class="container">
    <div class="logo"><a href="./index.jsp">滁州城西水库水源地保护规划管理平台-后台管理系统</a></div>
    <div class="left_open">
        <i title="展开左侧栏" class="iconfont">&#xe699;</i>
    </div>
    <%--<ul class="layui-nav left fast-add" lay-filter="">
        <li class="layui-nav-item">
            <a href="javascript:;">+新增</a>
            <dl class="layui-nav-child"> <!-- 二级菜单 -->
                <dd><a onclick="x_admin_show('资讯','http://www.baidu.com')"><i class="iconfont">&#xe6a2;</i>资讯</a></dd>
                <dd><a onclick="x_admin_show('图片','http://www.baidu.com')"><i class="iconfont">&#xe6a8;</i>图片</a></dd>
                <dd><a onclick="x_admin_show('用户','http://www.baidu.com')"><i class="iconfont">&#xe6b8;</i>用户</a></dd>
            </dl>
        </li>
    </ul>--%>
    <ul class="layui-nav right" lay-filter="">
        <li class="layui-nav-item">
            <a href="javascript:;">${user.u_name}</a>
            <dl class="layui-nav-child"> <!-- 二级菜单 -->
                <dd><a onclick="x_admin_show('个人信息','http://www.baidu.com')">个人信息</a></dd>
                <dd><a onclick="x_admin_show('切换帐号','http://www.baidu.com')">切换帐号</a></dd>
                <dd><a href="${pageContext.request.contextPath}/login.jsp">退出</a></dd>
            </dl>
        </li>
        <li class="layui-nav-item to-index"><a href="/">前台首页</a></li>
    </ul>

</div>
<!-- 顶部结束 -->
<!-- 中部开始 -->
<!-- 左侧菜单开始 -->
<div class="left-nav">
    <div id="side-nav">
        <ul id="nav">
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6b8;</i>
                    <cite>用户管理</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="${pageContext.request.contextPath}/admin/userList.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>用户列表</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="${pageContext.request.contextPath}/common/modifyPassword.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>修改密码</cite>
                        </a>
                    </li>
                    <li>
                        <!-- <a href="javascript:;">
                            <i class="iconfont">&#xe70b;</i>
                            <cite>会员管理</cite>
                            <i class="iconfont nav_right">&#xe697;</i>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a _href="xxx.jsp">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>会员列表</cite>
                                </a>
                            </li >
                            <li>
                                <a _href="xx.jsp">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>会员删除</cite>
                                </a>
                            </li>
                            <li>
                                <a _href="xx.jsp">
                                    <i class="iconfont">&#xe6a7;</i>
                                    <cite>等级管理</cite>
                                </a>
                            </li>
                        </ul> -->
                    </li>
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe723;</i>
                    <cite>信息查询</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="${pageContext.request.contextPath}/common/waterintakeList.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>取水口信息</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="${pageContext.request.contextPath}/common/channelList.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>渠道信息</cite>
                        </a>
                    </li >
                    <!-- <li>
                        <a _href="../common/">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>水质信息</cite>
                        </a>
                    </li > -->
                    <li>
                        <a _href="${pageContext.request.contextPath}/common/reserveList.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>保护区划分信息</cite>
                        </a>
                    </li >
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe726;</i>
                    <cite>地图操作</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="${pageContext.request.contextPath}/common/showAllGeoms.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>综合信息预览</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="${pageContext.request.contextPath}/common/showWaterintake.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>添加取水口信息</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="${pageContext.request.contextPath}/common/showChannel.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>添加渠道信息</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="${pageContext.request.contextPath}/common/showReserve.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>添加保护区信息</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="${pageContext.request.contextPath}/common/showReservoir.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>查看水库信息</cite>
                        </a>
                    </li >
                    <li>
                        <a _href="${pageContext.request.contextPath}/common/measure.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>面积和长度测量</cite>
                        </a>
                    </li >
                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="iconfont">&#xe6ce;</i>
                    <cite>数据统计</cite>
                    <i class="iconfont nav_right">&#xe697;</i>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a _href="${pageContext.request.contextPath}/common/data_statistics.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>水库数据统计</cite>
                        </a>
                    </li >
                    <!-- <li>
                        <a _href="echarts2.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>各保护区面积</cite>
                        </a>
                    </li>
                    <li>
                        <a _href="echarts3.jsp">
                            <i class="iconfont">&#xe6a7;</i>
                            <cite>水库总面积</cite>
                        </a>
                    </li> -->
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- <div class="x-slide_left"></div> -->
<!-- 左侧菜单结束 -->
<!-- 右侧主体开始 -->
<div class="page-content">
    <div class="layui-tab tab" lay-filter="xbs_tab" lay-allowclose="true">
        <ul class="layui-tab-title">
            <li class="layui-this" lay-allowclose="false">综合信息预览</li>
        </ul>
        <div class="layui-tab-content">
            <div class="layui-tab-item layui-show">
                <iframe tab-id="9" src='${pageContext.request.contextPath}/common/showAllGeoms.jsp' frameborder="0" scrolling="yes" class="x-iframe"></iframe>
            </div>
        </div>
    </div>
</div>
<div class="page-content-bg"></div>
<!-- 右侧主体结束 -->
<!-- 中部结束 -->
<!-- 底部开始 -->
<div class="footer">
    <div class="copyright">Copyright ©2020 HuPing All Rights Reserved</div>
</div>
<!-- 底部结束 -->
<script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/layui/layui.all.js" charset="utf-8"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/xadmin.js"></script>
</body>
</html>

<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>新增用户</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/js/layui/css/layui.css" media="all">
    <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/layui/layui.all.js" charset="utf-8"></script>
</head>
<body>
<div style="padding: 40px; background-color: #F2F2F2;">
    <div class="layui-card">
        <div class="layui-card-header" style="font-weight:bold;font-size: 18px;padding: 0px 100px">新增用户</div>
        <div class="layui-card-body newReserve">
            <form class="layui-form" method="post">
                <div class="layui-form-item">
                    <label class="layui-form-label">用户账号</label>
                    <div class="layui-input-inline">
                        <input type="text" name="u_id" lay-verify="required|name" placeholder="请输入用户账号"
                               autocomplete="on"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">用户姓名</label>
                    <div class="layui-input-inline">
                        <input type="text" name="u_name" lay-verify="required|name" placeholder="请输入用户姓名"
                               autocomplete="on"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">用户角色</label>
                    <div class="layui-input-inline">
                        <select name="u_role" lay-verify="required" lay-search="">
                            <option value="-1">请选择用户角色</option>
                            <option value="0">普通用户</option>
                            <option value="1">管理员</option>
                        </select>
                    </div>
                </div>
                <div class="layui-form-item">
                    <div class="layui-input-block">
                        <button class="layui-btn" lay-submit lay-filter="formDemo">保存</button>
                        <!-- <button type="reset" class="layui-btn layui-btn-primary">重置</button> -->
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script>
    //Demo
    layui.use('form', function () {
        var form = layui.form;
        form.render();

        //监听提交
        form.on('submit(formDemo)', function (data) {
            //layer.msg(JSON.stringify(data.field));
            $.ajax({
                url:"${pageContext.request.contextPath}/addUser.action",
                async: false,
                type:"POST",
                dataType: "json",
                data:data.field,
                success: function(obj){
                    if (obj.ifSucceed == true) {
                        parent.location.reload();
                        parent.parent.layer.msg("添加成功！");
                    } else {
                        layer.msg("添加失败！");
                    }
                }
            });
            return false;
        });

        form.verify({
            id: function(value, item){ //value：表单的值、item：表单的DOM对象
                if(!new RegExp("^[a-zA-Z0-9]+$").test(value)){
                    return '分系统编号只能由字母或数字组成';
                }
                if (value.length < 5) {
                    return '分系统编号长度不能小于5个字符';
                }
                if (value.length > 20) {
                    return '分系统编号长度不能大于20个字符';
                }
                //自定义ajax校验规则
                var result = '';
                $.ajax({
                    type: "POST",
                    url: "${pageContext.request.contextPath}/user/checkSubsystem_idIfExist.action",
                    data:{"subs_id":value},
                    /*contentType:"application/json;charset=UTF-8",*/
                    async: false,
                    dataType: "json",
                    success: function (flag) {
                        if (flag.ifExist == true) {
                            result = '已存在该分系统编号';
                            console.log(result);
                        }
                    },
                    error: function () {

                    }
                });
                if (result != '') {
                    return '已存在该账号';
                }
            }
            ,name: function (value, item) {
                if (!/^[a-zA-Z0-9\u4e00-\u9fa5]+$/.test(value)) {
                    return '分系统名称不能包含特殊字符';
                }
            }
        });
    });
</script>
</body>
</html>

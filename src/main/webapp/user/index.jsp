<%--
  Created by IntelliJ IDEA.
  User: zhourenxi
  Date: 2020/4/13
  Time: 14:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title> 滁州城西水库水源地保护规划管理平台 </title>

    <meta charset="utf-8">
    <meta name="description" content="滁州城西水库水源地保护规划！" />
    <meta name="keywords" content="滁州城西水库水源地保护规划" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/js/layui/css/layui.css" />
    <style>
        *{
            margin: 0;
            padding: 0;
        }
        body {
            font-family: "Microsoft YaHei";
            font-size: 12px;
            color: #ffffff;
            /*margin: 0px auto;*/
        }

        .head {
            position:relative;
            width: 100%;
            height: 100px;
            background: #ffffff;
        }

        .head .logo {
            position: absolute;
            display: block;
            text-decoration: none;
            width: 100px;
            height: 80px;
            top: 20px;
            left: 30px;
            background: url(${pageContext.request.contextPath}/images/logo.png) no-repeat;
        }
        .head .user-headface {
            position: absolute;
            float: right;
            width: 75px;
            height: 75px;
            overflow: hidden;
            border-radius: 50%;
            right: 240px;
            top: 10px;
        }

        .head .user-account {
            position: absolute;
            float: right;
            top: 40px;
            right:40px;
            padding-left: 20px;
            color: #666;
        }

        .head .logotitle{
            position: absolute;
            display:block;
            font-family:"Microsoft YaHei";
            font-weight: 500;
            color: #000000;
            left: 140px;
            top: 30px;
            font-size:25px;
        }

        nav ul {
            z-index: 1;
            list-style: none;
            padding-left: 0;
            margin-top: 0;
            margin-bottom: 0;
        }

        nav ul li {
            width: 220px;
        }

        .nav {
            width: 100%;
            margin: 0 auto;
            background: #0080ef;
            color: #f0f0f0;
        }

        .nav a {
            display: block;
            padding: 0 16px;
            line-height: inherit;
            cursor: pointer;
        }

        .nav_menu {
            line-height: 45px;
            font-weight: 800;
            text-transform: uppercase;
            text-align: center;
            font-size: 15px;
        }

        .nav_menu-item {
            display: inline-block;
            position: relative;
        }

        .nav_menu-item:hover {
            background-color: #0099ff;
        }

        .nav_menu-item:hover .nav_submenu {
            display: block;
        }

        .nav_submenu {
            font-weight: 300;
            text-transform: none;
            display: none;
            position: absolute;
            width: 220px;
            background-color: #99CCFF;
        }

        .nav_submenu-item:hover {
            background: rgba(0, 0, 0, 0.3);
        }

        #iframe_td_id {
            width:100%;
            height:100%;
        }

        a,
        a:hover {
            color: #FFF;
            text-decoration: none
        }

        .nav_submenu-item .a1,
        .a1:hover {
            color: #FFF;
            text-decoration: none
        }

        *,
        *:before,
        *:after {
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
    </style>
</head>
<body>

<div class="head">

    <a class="logo" href="${pageContext.request.contextPath}/user/index.jsp" title="滁州城西水库水源地保护规划管理平台"></a>
    <a class="logotitle" href="${pageContext.request.contextPath}/user/index.jsp" title="滁州城西水库水源地保护规划管理平台">滁州城西水库水源地保护规划系统</a>

    <div class="user-headface">
        <img src="${pageContext.request.contextPath}/images/user_face.jpg">
    </div>
    <div class="user-account">
        <p class="account">
            <span>帐户名：${user.u_id}</span>
            <span>用户：${user.u_name}</span>
        </p>
    </div>
</div>

<nav class="nav">
    <ul class="nav_menu">
        <li class="nav_menu-item">
            <a href="${pageContext.request.contextPath}/user/index.jsp">首页</a>
        </li>
        <li class="nav_menu-item">
            <a href="javascript:;">信息查询</a>
            <ul class="nav_submenu">
                <li class="nav_submenu-item">
                    <a href="${pageContext.request.contextPath}/common/showWaterintake.jsp" target="mainFrame">取水口</a>
                </li>
                <li class="nav_submenu-item">
                    <a href="${pageContext.request.contextPath}/common/showChannel.jsp" target="mainFrame">渠道</a>
                </li>
                <li class="nav_submenu-item">
                    <a href="${pageContext.request.contextPath}/common/showReserve.jsp" target="mainFrame">保护区</a>
                </li>
                <li class="nav_submenu-item">
                    <a href="${pageContext.request.contextPath}/common/showReservoir.jsp" target="mainFrame">水库</a>
                </li>
                <li class="nav_submenu-item">
                    <a href="${pageContext.request.contextPath}/common/showAllGeoms.jsp" target="mainFrame">综合</a>
                </li>
            </ul>
        </li>
        <li class="nav_menu-item">
            <a href="${pageContext.request.contextPath}/common/modifyPassword.jsp">信息修改</a>
            <ul class="nav_submenu">

            </ul>
        </li>
        <li class="nav_menu-item">
            <a href="${pageContext.request.contextPath}/common/measure.jsp" target="mainFrame">测量工具</a>
            <ul class="nav_submenu">

            </ul>
        </li>
        <li class="nav_menu-item">
            <a href="${pageContext.request.contextPath}/common/data_statistics.jsp" target="mainFrame">数据统计</a>
            <ul class="nav_submenu">
                <%--<li class="nav_submenu-item">
                    <a class="a1" href="${pageContext.request.contextPath}/common/" target="mainFrame">取水口数量</a>
                </li>
                <li class="nav_submenu-item">
                    <a class="a1" onclick="upurlifarme('https://www.bilibili.com/')" target="_blank">保护区面积</a>
                </li>
                <li class="nav_submenu-item">
                    <a class="a1" onclick="upurlifarme('https://www.bilibili.com/')" target="_blank">水库总面积</a>
                </li>--%>
            </ul>
        </li>
    </ul>
</nav>

<!--你们的内容就在这里写可以自己定义的<div>你们的内容</div>-->

</ul>
</div>

<div id="iframe_td_id">
    <iframe id="IframeID'" name="mainFrame" style="padding: 0;" height="1000px" width="100%" frameborder="0" border="0" marginwidth="0" marginheight="0" scrolling="no" src="${pageContext.request.contextPath}/user/welcome.jsp"></iframe>
</div>
<%--<div class="footer-befer"><img src="../images/footerbeofer.png"></div>
<div class="footer-after">
    <p class="pt">
        Copyright © 1998-2016 Tencent. All Rights Reserved. &nbsp;&nbsp;
    </p>
</div>--%>

<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/layui/layui.all.js" ></script>
</body>

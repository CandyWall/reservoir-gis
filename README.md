# 水源地保护规划管理平台

## 系统技术选型

* 前端：使用Openlayers+layui+echarts
* 后端：jsp+springboot+postgis
* 地图：天地图

## 系统部分截图

### 图层的选择

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210415003044568.gif#pic_center)

### 在地图上标注点

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210415003044718.gif#pic_center)

### 在地图上绘制直线和曲线

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210415003314472.gif#pic_center)

### 在地图上标规则面和不规则面

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210415003045495.gif#pic_center)

### 在地图上测算距离和面积

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210415003042843.gif#pic_center)

### echarts统计图

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210415003042214.gif#pic_center)

### Layui数据表格

![在这里插入图片描述](https://img-blog.csdnimg.cn/20210415003042377.gif#pic_center)